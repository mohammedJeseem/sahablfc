package com.example.admin.stockmanagementapp.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetAllProductEvent;
import com.example.admin.stockmanagementapp.ListProductActivity;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class GetProductsTask extends AsyncTask<GetProductsTask.Params, Void, ArrayList<Product>> {

    Context mContext;

    public GetProductsTask(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    protected void onPostExecute(ArrayList<Product> products) {
        super.onPostExecute(products);
        EventBus.getDefault().post(new GetAllProductEvent(products));
    }

    @Override
    protected ArrayList<Product> doInBackground(Params... params) {

        ArrayList<Product> products = new ArrayList<Product>();

        if (params.length < 1) {
            return  (ArrayList<Product>) AppConstants.getApplicationDB().daoAccess().fetchAllProducts();
        }

        if (params[0].category_id != -1 && !params[0].search_key.equals("")) {
            products = (ArrayList<Product>) AppConstants.getApplicationDB().daoAccess().getProductByIdAndKey(params[0].category_id, params[0].search_key);
        } else if (params[0].category_id != -1) {
            products = (ArrayList<Product>) AppConstants.getApplicationDB().daoAccess().getProductByCategoryId(params[0].category_id);
        } else if (!params[0].search_key.equals("")) {
            products = (ArrayList<Product>) AppConstants.getApplicationDB().daoAccess().getProductByKey(params[0].search_key);
        } else {
            products = (ArrayList<Product>) AppConstants.getApplicationDB().daoAccess().fetchAllProducts();
        }

        return products;
    }

    public static class Params {
        private int category_id;
        private String search_key;

        public Params(int category_id, String search_key) {
            this.category_id = category_id;
            this.search_key = search_key;
        }
    }
}
