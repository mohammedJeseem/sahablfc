package com.example.admin.stockmanagementapp.Utils;

import com.example.admin.stockmanagementapp.Entities.Product;

import java.util.ArrayList;

public class CartAddedItem {
    private static CartAddedItem cartAddedItem;
    private ArrayList<Product> addedProducts;

    public static CartAddedItem getCartAddedItem() {

        if (null == cartAddedItem) {
            cartAddedItem = new CartAddedItem();
        }
        return cartAddedItem;
    }

    public ArrayList<Product> getAddedProducts() {
        return addedProducts;
    }

    public void setAddedProducts(ArrayList<Product> addedProducts) {
        if (addedProducts == null) {
            addedProducts = new ArrayList<>();
        }
        this.addedProducts = addedProducts;
    }

    public void addProductToCart(Product mProduct) {

        if (addedProducts == null) {
            addedProducts = new ArrayList<>();
        }

        boolean alreadyAdded = false;
        for (Product product: addedProducts) {
            if (product.getProductId() == mProduct.getProductId())
                alreadyAdded = true;
        }

        if (!alreadyAdded)
            addedProducts.add(mProduct);

    }


    public void removeProductFromCart(int productId) {
        if(addedProducts == null || addedProducts.size() == 0) return;
        for (int i = 0; i < addedProducts.size(); i++) {
            if(addedProducts.get(i).getProductId() == productId) {
                addedProducts.remove(i);
            }
        }
    }

    public void clearCartItem() {
        addedProducts.clear();
        addedProducts = null;
    }

    public int getProductCount() {
        if (addedProducts != null)
            return this.addedProducts.size();
        else
            return 0;
    }

    public void setCartItemQuantity(int productId, float quantity) {
        for (Product product: addedProducts) {
            if (product.getProductId() == productId)
                product.setQuantity(quantity);
        }
    }

    public float getTotalprice() {
        if (addedProducts == null) return 0;
        float total = 0;
        for (Product product: addedProducts) {

            total += product.getQuantity() * product.getPrice();
        }

        return total;
    }

    public float getQuantityById(int id) {
        float quantity = 1;
        if(addedProducts != null && addedProducts.size() > 0) {
            for (int i = 0; i < addedProducts.size(); i++) {
                if (id == addedProducts.get(i).getProductId()){
                    quantity = addedProducts.get(i).getQuantity();
                }
            }
        }
        return quantity;
    }
}
