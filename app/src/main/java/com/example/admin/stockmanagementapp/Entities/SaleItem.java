package com.example.admin.stockmanagementapp.Entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class SaleItem {

    @PrimaryKey(autoGenerate = true)
    private int saleItemId;

    private int productId;
    private long saleId;
    private float quantity;

    public SaleItem(int saleId, int productId, float quantity) {
        this.productId = productId;
        this.saleId = saleId;
        this.quantity = quantity;
    }

    public void setSaleItemId(int saleItemId) {
        this.saleItemId = saleItemId;
    }

    public int getProductId() {
        return productId;
    }

    public long getSaleId() {
        return saleId;
    }

    public float getQuantity() {
        return quantity;
    }

    public int getSaleItemId() {
        return saleItemId;
    }

    public void setSaleId(long saleId) {
        this.saleId = saleId;
    }
}
