package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.databinding.ActivityAddCategoryBinding;

public class AddCategory extends AppCompatActivity {

	ActivityAddCategoryBinding mBinding;
	Category mCategory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_category);
		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_category);

		Toolbar myToolbar = mBinding.addCategoryToolbar;
		myToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
		myToolbar.setTitle("Category Add");
		setSupportActionBar(myToolbar);

		mBinding.addCategorySaveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				saveCategory();
			}
		});

	}

	private void saveCategory() {
		String category = mBinding.addCategoryEt.getText().toString();
		if(category.isEmpty()){
			Toast.makeText(getApplicationContext(),"Please enter category",Toast.LENGTH_LONG).show();
			return;
		}
		mCategory = new Category(category);
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				AppConstants.getApplicationDB().daoAccess().insertCategory(mCategory);
			}
		});
		thread.start();

		mBinding.addCategoryEt.getText().clear();
		Toast.makeText(getApplicationContext(),"Category Added",Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return true;
	}

	@Override
	public void onBackPressed() {

		super.onBackPressed();
		Intent intent = new Intent(this,ProductActivity.class);
		intent.putExtra("add_product", true);
		startActivity(intent);
		finish();
	}

}
