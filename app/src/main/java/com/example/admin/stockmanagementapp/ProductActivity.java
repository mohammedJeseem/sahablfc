package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Adapters.CategoryArrayAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.DeleteProductTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.SaveProductTask;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.DBChangeEvent;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.RefreshListEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.MediaUtil;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.ActivityProductBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;

public class ProductActivity extends AppCompatActivity {

    private static final int GALLERY = 101;
    private static final int CAMERA = 102;
    private static final int MENU_EDIT_ID = 201;
    private static final int MENU_CATEGORY_ID = 202;
    private static final int MENU_ADD_ID = 203;
    private static final int MENU_DELETE_ID = 204;
    private static final int MENU_SHARE_ID = 205;
    ActivityProductBinding mBinding;
    Product mProduct;
    String imageFilePath;
    int product_id = 0;
    RequestOptions requestOptions;
    int productMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product);
        requestOptions = new RequestOptions()
                .transforms(new CenterCrop()
                        , new RoundedCorners((int) Utility.dpToPx(this, 2)));

        populateCategoryMenu();

        if (getIntent().getSerializableExtra("product") != null) {
            mProduct = (Product) getIntent().getSerializableExtra("product");
            productMode = PRODUCT_MODE.VIEW;
            product_id = mProduct.getProductId();
            imageFilePath = mProduct.getImage_path();
        }

        if (getIntent().getBooleanExtra("add_product", false)) {
            initUIforAdd();
        } else {
            initUI();
        }

        handleActionButton();
//        categoryTest();
        setUnitSpinner();
    }

    private void populateCategoryMenu() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }

    private void handleActionButton() {
        mBinding.cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        mBinding.galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImageFromGallery();
            }
        });

        mBinding.productSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveProduct();
            }
        });

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBinding.toolbarTitle.getText().toString() == "EDIT PRODUCT"){
                    initUI();
                    return;
                }
                if(mBinding.toolbarTitle.getText().toString() == "ADD PRODUCT"){onBackPressed();}
                if(mBinding.toolbarTitle.getText().toString() == "VIEW PRODUCT"){onBackPressed();}
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(ProductActivity.this, ListProductActivity.class);
//        startActivity(intent);
//        finish();
    }
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        if (productMode == PRODUCT_MODE.ADD) {
            popup.getMenu().add(1, MENU_CATEGORY_ID, 1, "View/Add Category");
        } else if (productMode == PRODUCT_MODE.VIEW) {
            popup.getMenu().add(1, MENU_EDIT_ID, 1, "Edit");
//            popup.getMenu().add(1, MENU_DELETE_ID, 1, "Delete");
            popup.getMenu().add(1, MENU_SHARE_ID, 1, "Share");
            popup.getMenu().add(1, MENU_ADD_ID, 1, "Add product");
        } else {
            popup.getMenu().add(1, MENU_CATEGORY_ID, 1, "View/Add Category");
        }


//        popup.getMenu().add(1, 103, 1, "Menu3");
//        popup.getMenu().add(1, 104, 1, "Menu3");
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case MENU_CATEGORY_ID:
                       startCategoryActivity();
                        return true;
                    case MENU_ADD_ID:
                        initUIforAdd();
                        return true;

                    case MENU_EDIT_ID:
                        configUIforEdit();
                        return true;

                    case MENU_DELETE_ID:
//                        deleteProduct();
                        return true;

                    case MENU_SHARE_ID:
                        shareWhatsapp();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void startCategoryActivity() {
        Intent intent = new Intent(this, ListCategoryActivity.class);
        startActivity(intent);
        finish();
    }



    private void initUI() {
        mBinding.productName.setText(mProduct.getName());
        mBinding.productPrice.setText(mProduct.getPrice().toString());

        mBinding.coverPicImageLayout.setVisibility(View.GONE);
        mBinding.selectImageCoverLabel.setVisibility(View.GONE);
        mBinding.coverPic.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(mProduct.getImage_path())
                .apply(requestOptions)
                .into(mBinding.coverPic);



        mBinding.productName.setEnabled(false);
        mBinding.productPrice.setEnabled(false);
        mBinding.productCategory.setEnabled(false);
        mBinding.productWeight.setEnabled(false);

        mBinding.toolbarTitle.setText("VIEW PRODUCT");

    }

    private void initUIforAdd() {
        productMode = PRODUCT_MODE.ADD;
        clearValues();

        mBinding.productName.setEnabled(true);
        mBinding.productPrice.setEnabled(true);
        mBinding.productCategory.setEnabled(true);
        mBinding.productWeight.setEnabled(true);
        mBinding.productSaveBtn.setVisibility(View.VISIBLE);
        mBinding.coverPicBtnLayout.setVisibility(View.VISIBLE);

        mBinding.toolbarTitle.setText("ADD PRODUCT");
    }

    private void configUIforEdit() {
        productMode = PRODUCT_MODE.EDIT;
        mBinding.productName.setEnabled(true);
        mBinding.productPrice.setEnabled(true);
        mBinding.productCategory.setEnabled(true);
        mBinding.productWeight.setEnabled(true);
        mBinding.productSaveBtn.setText("UPDATE");
        mBinding.productSaveBtn.setVisibility(View.VISIBLE);
        mBinding.coverPicBtnLayout.setVisibility(View.VISIBLE);

        mBinding.toolbarTitle.setText("EDIT PRODUCT");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;
        if (requestCode == GALLERY && data.getData() != null) {
            Bitmap bitmap = MediaUtil.decodeBitmap(this.getApplicationContext(), data.getData(), 1280, 720);
            setSelectedImage(bitmap);
        } else if (requestCode == CAMERA) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            setSelectedImage(photo);
        }
    }

    private void chooseImageFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        galleryIntent.setType("image/*");
        galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*"});
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void openCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, CAMERA);
    }

    private void setSelectedImage(Bitmap bitmap) {
//        Bitmap bitmap = MediaUtil.decodeBitmap(this.getApplicationContext(), uri, 1280, 720);
        imageFilePath = getExternalFilesDir(null) + File.separator + "product_images" + File.separator  + "item" + UUID.randomUUID().toString() + ".png";

        try {
            File outputFile = new File(imageFilePath);
            if (!outputFile.getParentFile().exists())
                outputFile.getParentFile().mkdirs();

            outputFile.createNewFile();

            FileOutputStream out = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

            mBinding.coverPicImageLayout.setVisibility(View.GONE);
            mBinding.selectImageCoverLabel.setVisibility(View.GONE);
            mBinding.coverPic.setVisibility(View.VISIBLE);
            Glide.with(this).load(outputFile)
                    .apply(requestOptions)
                    .into(mBinding.coverPic);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveProduct() {

        String name = mBinding.productName.getText().toString();
        String amount = mBinding.productPrice.getText().toString();
        if(name.isEmpty() || amount.isEmpty()){
            Toast.makeText(getApplicationContext(),"Error in product name or price",Toast.LENGTH_LONG).show();
            return;
        }
        int category_id = ((Category)mBinding.productCategory.getSelectedItem()).getCategoryId();
        String categoryName = ((Category)mBinding.productCategory.getSelectedItem()).getName();

        Float price = Float.valueOf(amount);
        String unit = mBinding.productWeight.getSelectedItem().toString();
        mProduct = new Product(category_id, name, imageFilePath, unit, price);
        mProduct.setCategory_name(categoryName);

        boolean isUpdate = false;
        if (productMode == PRODUCT_MODE.EDIT) {
            isUpdate = true;
            mProduct.setProductId(product_id);
        }
        SaveProductTask task = new SaveProductTask(isUpdate);
        task.execute(mProduct);
    }
//    private void categoryTest() {
//        ArrayList<Category> categories = new ArrayList<>();
//        for (int i = 1; i < 10 ; i++ ) {
//            Category category = new Category("Category "+i);
//            category.setCategoryId(i);
//            categories.add(category);
//        }
//
//        setCategorySpinner(categories);
//    }
    private void setCategorySpinner(ArrayList<Category> categories) {

        ArrayAdapter<Category> adapter =
                new CategoryArrayAdapter(this, categories);
//        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        mBinding.productCategory.setAdapter(adapter);

        mBinding.productCategory.setSelection(getCategorySelectPosition(categories));
    }

    private int getCategorySelectPosition(ArrayList<Category> categories) {
        if(mProduct == null) return 0;
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getCategoryId() == mProduct.getCategoryId()) {
                return i;
            }
        }
        return 0;
    }

    private void setUnitSpinner() {
        String spinnerArray[] = { AppConstants.Unit.NUMBER,AppConstants.Unit.KILOGRAM};
        ArrayAdapter<String> unitSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                spinnerArray);
        unitSpinnerAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mBinding.productWeight.setAdapter(unitSpinnerAdapter);
        mBinding.productWeight.setSelection(getUnitSelectposition(spinnerArray));
    }

    private int getUnitSelectposition(String[] units) {
        if(mProduct == null) return 0;
        for (int i = 0; i < units.length; i++) {
            if (units[i].equals(mProduct.getUnit())) {
                return i;
            }
        }
        return 0;
    }

    private void clearValues() {
        mBinding.productName.getText().clear();
        mBinding.productPrice.getText().clear();
        mBinding.productCategory.setSelection(0);
        mBinding.coverPic.setImageResource(0);
        mProduct = null;
    }

    private void afterUpdate() {
        productMode = PRODUCT_MODE.VIEW;
        mBinding.coverPicImageLayout.setVisibility(View.GONE);
        mBinding.selectImageCoverLabel.setVisibility(View.GONE);
        mBinding.coverPicBtnLayout.setVisibility(View.GONE);
        mBinding.coverPic.setVisibility(View.VISIBLE);

        mBinding.productName.setEnabled(false);
        mBinding.productPrice.setEnabled(false);
        mBinding.productCategory.setEnabled(false);
        mBinding.productWeight.setEnabled(false);

        mBinding.productSaveBtn.setVisibility(View.GONE);
        mBinding.toolbarTitle.setText("VIEW PRODUCT");
    }

    private void deleteProduct() {
        DeleteProductTask task = new DeleteProductTask();
        task.execute(mProduct);
    }

    private void shareWhatsapp() {
        /**
         * Show share dialog BOTH image and text
         */
        Uri imageUri = Uri.parse(imageFilePath);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        //Target whatsapp:
        shareIntent.setPackage("com.whatsapp");

        String sentText = mProduct.getName() + "\nRs " +mProduct.getPrice() + "/- per " + mProduct.getUnit();
        //Add text and then Image URI
        shareIntent.putExtra(Intent.EXTRA_TEXT, sentText);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
//            startActivity(shareIntent);
            startActivity(Intent.createChooser(shareIntent, "Share image via:"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onCategoryLoaded(GetAllCategoryEvent event) {
        setCategorySpinner(event.getCategories());
    }

    @Subscribe
    public void onProductChange(DBChangeEvent event) {
        if (event.getType() == DBChangeEvent.PRODUCT_INSERT) {
            if(event.isSuccess()) {
                Toast.makeText(this, "Product added", Toast.LENGTH_SHORT).show();
                clearValues();
            } else {
                Toast.makeText(this, "Unable to add product !", Toast.LENGTH_SHORT).show();
            }
        } else if (event.getType() == DBChangeEvent.PRODUCT_UPDATE) {
            if(event.isSuccess()) {
                Toast.makeText(this, "Product updated", Toast.LENGTH_SHORT).show();
                afterUpdate();
            } else {
                Toast.makeText(this, "Unable to update product !", Toast.LENGTH_SHORT).show();
            }
        } else if (event.getType() == DBChangeEvent.PRODUCT_DELETE) {
            if (event.isSuccess()) {
                Toast.makeText(this, "Product deleted", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(this, "Unable to delete product !", Toast.LENGTH_SHORT).show();
            }
        }

        EventBus.getDefault().postSticky(new RefreshListEvent(RefreshListEvent.REFRESH_PRODUCT));
    }

    public static class PRODUCT_MODE {
        public static int VIEW = 1;
        public static int EDIT = 2;
        public static int ADD = 3;
    }


}
