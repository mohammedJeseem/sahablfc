package com.example.admin.stockmanagementapp.Events;

public class RefreshListEvent {
    public static int REFRESH_PRODUCT = 0;
    public static int REFRESH_CATEGORY = 1;

    private int type;

    public RefreshListEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
