package com.example.admin.stockmanagementapp.Events;

public class GetBillDetailsEvent {

    private String billNo;
    private Boolean isLongClickEvent;

    public GetBillDetailsEvent(String billNo, Boolean isLongClickEvent) {
        this.billNo = billNo;
        this.isLongClickEvent = isLongClickEvent;
    }

    public String getBillNo() {
        return billNo;
    }
    public Boolean getIsLongClickEvent () {
        return isLongClickEvent;
    }


}
