package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.CategoryArrayAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListSaleProductAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetProductsTask;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.GetAllProductEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.Events.OnProductAddedCartEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.AppPrefs;
import com.example.admin.stockmanagementapp.Utils.BasketManager;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.databinding.ActivitySaleBinding;
import com.mancj.materialsearchbar.MaterialSearchBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class SaleActivity extends AppCompatActivity {

    ActivitySaleBinding mBinding;
    ListSaleProductAdapter mAdapter;
    MaterialSearchBar materialSearchBar;
    List<String> suggestList;

    Boolean isSaleTypeGroup;

    private ArrayList<Sale> saleItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_sale);

        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_sale);
        mAdapter = new ListSaleProductAdapter(this);

        mBinding.listProductSale.setAdapter(mAdapter);
        mBinding.listProductSale.setLayoutManager(new GridLayoutManager(this,1));
        suggestList = new ArrayList<>();


        if (getIntent().hasExtra(AppConstants.SALE_TYPE) && getIntent().getExtras().getBoolean(AppConstants.SALE_TYPE)) {
            isSaleTypeGroup = true;
        } else {
            isSaleTypeGroup = false;
        }


        getProducts(-1,"");
        setRefreshLayout();
        handleButtons();
        populateCategoryMenu();
        updateCartBadge();

    }
    private void populateCategoryMenu() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }
//    private void categoryTest() {
//        ArrayList<Category> categories = new ArrayList<>();
//        for (int i = 1; i < 10 ; i++ ) {
//            Category category = new Category("Category "+i);
//            category.setCategoryId(i);
//            categories.add(category);
//        }
//
//        setCategorySpinner(categories);
//    }

    private void setCategorySpinner(ArrayList<Category> categories) {

        Category emptyCategoryItem = new Category("--category--");
        emptyCategoryItem.setCategoryId(-1);
        categories.add(0, emptyCategoryItem);

        ArrayAdapter<Category> adapter =
                new CategoryArrayAdapter(this, categories);

        mBinding.saleCategorySpinner.setAdapter(adapter);
        //mBinding.saleCategorySpinner.setSelection(getCategorySelectPosition(categories));

        mBinding.saleCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int category_id = ((Category)mBinding.saleCategorySpinner.getSelectedItem()).getCategoryId();
                getProducts(category_id, mBinding.saleSearchBar.getText().toLowerCase());
                Log.d("Vishnu", "onItemSelected: "+((Category)mBinding.saleCategorySpinner.getSelectedItem()).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getProductByCategory(int category_id) {

    }

    private int getCategorySelectPosition(ArrayList<Category> categories) {
//        if(mProduct == null) return 0;
//        for (int i = 0; i < categories.size(); i++) {
//            if (categories.get(i).getCategoryId() == mProduct.getCategoryId()) {
//                return i;
//            }
//        }
        return 0;
    }

    private void handleButtons(){

        mBinding.saleCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.putExtra(AppConstants.SALE_TYPE, isSaleTypeGroup);
                startActivity(intent);
//                finish();
            }
        });

        mBinding.viewCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.putExtra(AppConstants.SALE_TYPE, isSaleTypeGroup);
                startActivity(intent);
            }
        });

        mBinding.backButtonSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        materialSearchBar = findViewById(R.id.sale_search_bar);
        materialSearchBar.hideSuggestionsList();
//        materialSearchBar.setLastSuggestions(suggestList);
//        materialSearchBar.setMaxSuggestionCount(4);
        materialSearchBar.setCardViewElevation(10);
        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                List<String> suggest = new ArrayList<String>();
                for(String search : suggestList){
                    if(search.toLowerCase().contains(materialSearchBar.getText().toLowerCase())){
                        suggest.add(search);
                    }
//                    materialSearchBar.setLastSuggestions(suggest);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                String keyword = "%" + s.toString().toLowerCase() + "%";
                getProducts(((Category) mBinding.saleCategorySpinner.getSelectedItem()).getCategoryId(),keyword);
            }
        });

        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

                if(!enabled){
                    mBinding.listProductSale.setAdapter(mAdapter);
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {

                startSearch(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    private void startSearch(CharSequence text) {
        //get search product from db
    }

    private void setRefreshLayout() {
        mBinding.refreshLayoutSale.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProducts(-1, "");
            }
        });
    }

    private void getProducts(int category_id, String keyword) {
        GetProductsTask task = new GetProductsTask(this);
        task.execute(new GetProductsTask.Params(category_id, keyword));
    }

    private void updateCartBadge() {

        int count = 0;
        if (isSaleTypeGroup) {
            count = BasketManager.getBasketManager().getCurrentBasketCount();
            if(count == 0) {
                ArrayList<Product> basketAddedItems = AppPrefs.getInstance(this).getSaleGroupBasketItems(BasketManager.getBasketManager().getCurrentBasket().getName());
                for (Product mProduct : basketAddedItems) {
                    BasketManager.getBasketManager().getCurrentBasket().addProductToCart(mProduct);
                }
                count = basketAddedItems.size();
            }

        } else  {
            count = CartAddedItem.getCartAddedItem().getProductCount();

            if(count == 0) {
              ArrayList<Product> saleCartItems = AppPrefs.getInstance(this).getSaleCartItems();
              for (Product mProduct : saleCartItems) {
                  CartAddedItem.getCartAddedItem().addProductToCart(mProduct);
              }
              count = saleCartItems.size();
            }
        }

        if(count < 1) {
            mBinding.cartBadgeCount.setVisibility(View.GONE);
            return;
        }
        if (count < 10) {
            mBinding.cartBadgeCount.setText("0"+String.valueOf(count));
        } else {
            mBinding.cartBadgeCount.setText(String.valueOf(count));
        }
        mBinding.cartBadgeCount.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCartBadge();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(isSaleTypeGroup) {
            Intent intent = new Intent(this, SaleGroupActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Subscribe
    public void onGetProductEvent(GetAllProductEvent event) {
        if (event.getProducts().size() < 1) {
            Toast.makeText(this, "No Products", Toast.LENGTH_LONG).show();
        }
        mBinding.refreshLayoutSale.setRefreshing(false);
        mAdapter.setProducts(event.getProducts());

        for(int i=0; i < event.getProducts().size(); i++){
           suggestList.add(event.getProducts().get(i).getName());
        }
    }

    @Subscribe
    public void onProductItemClicked(OnProductAddedCartEvent event) {

        if (isSaleTypeGroup) {
            BasketManager.getBasketManager().addProductToCurrentBasket(event.getProduct());
            ArrayList<Product> basketAddedItems = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleGroupBasketItems(basketAddedItems, BasketManager.getBasketManager().getCurrentBasket().getName());
        } else {
            CartAddedItem.getCartAddedItem().addProductToCart(event.getProduct());
            ArrayList<Product> cartAddedItems = CartAddedItem.getCartAddedItem().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleCartItems(cartAddedItems);
        }

        updateCartBadge();

    }

    @Subscribe
    public void onCategoryLoaded(GetAllCategoryEvent event) {
        setCategorySpinner(event.getCategories());
    }

    @Subscribe
    public void onCartItemChanged(OnCartItemChanged event) {
        updateCartBadge();
    }


}
