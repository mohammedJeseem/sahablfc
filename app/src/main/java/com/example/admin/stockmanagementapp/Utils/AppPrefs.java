package com.example.admin.stockmanagementapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class AppPrefs {

    public static final String              LAST_BILL_NO = "last_bill_number";
    public static final String              LAST_COUNTER_NO = "last_counter_number";
    public static final String              LAST_COUNTER_DATE = "last_counter_date";
    public static final String              SHOP_NAME = "shop_name";
    public static final String              SHOP_PHONE = "shop_phone";
    public static final String              SHOP_ADDRESS = "shop_address";
    public static final String              PRINTER_NAME = "printer_name";
    public static final String              SHOP_GST = "shop_gst";
    public static final String              SHOP_TIN = "shop_gst_tin";
    public static final String              SALE_SAVE_CART = "sale_save_cart";
    private static AppPrefs                 mAppPrefs;
    private SharedPreferences prefs;

    private AppPrefs(Context context) {

        prefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public static AppPrefs getInstance(Context context) {

        if (mAppPrefs == null) {

            mAppPrefs = new AppPrefs(context);
        }

        return mAppPrefs;
    }

    public Long getLastBillNo() {
        return mAppPrefs.prefs.getLong(LAST_BILL_NO, 0000l);
    }

    public void updateLastBillNo() {
        long bill_no = getLastBillNo() + 1l;
        mAppPrefs.prefs.edit().putLong(LAST_BILL_NO, bill_no).apply();
    }

    public void iniBillNo() {
        mAppPrefs.prefs.edit().putLong(LAST_BILL_NO, 1001L).apply();
    }


    public Long getLastCounterNo() {
        return mAppPrefs.prefs.getLong(LAST_COUNTER_NO, 0000l);
    }

    public void updateLastCounterNo() {
        long counter_no = getLastCounterNo() + 1l;
        mAppPrefs.prefs.edit().putLong(LAST_COUNTER_NO, counter_no).apply();
    }
    public void clearCounterNo() {
        long counter_no = 1l;
        mAppPrefs.prefs.edit().putLong(LAST_COUNTER_NO, counter_no).apply();
    }

    public void iniCounterNo() {
        mAppPrefs.prefs.edit().putLong(LAST_COUNTER_NO, 1L).apply();
    }


    public void iniLastDateNo( long timeStamp) {
        mAppPrefs.prefs.edit().putLong(LAST_COUNTER_DATE, timeStamp).apply();
    }

    public Long getLastDateNo() {
        return mAppPrefs.prefs.getLong(LAST_COUNTER_DATE, 0000l);
    }

    public void saveShopSettings(String name, String phone, String address, String gst, String tin){

        mAppPrefs.prefs.edit().putString(SHOP_NAME,name).apply();
        mAppPrefs.prefs.edit().putString(SHOP_PHONE,phone).apply();
        mAppPrefs.prefs.edit().putString(SHOP_ADDRESS,address).apply();
        mAppPrefs.prefs.edit().putString(SHOP_GST,gst).apply();
        mAppPrefs.prefs.edit().putString(SHOP_TIN,tin).apply();

    }

    public void savePrinterName(String printerName){
        mAppPrefs.prefs.edit().putString(PRINTER_NAME, printerName).apply();
    }

    public String getShopName() {
        return mAppPrefs.prefs.getString(SHOP_NAME, "Name");
    }
    public String getShopPhone() {
        return mAppPrefs.prefs.getString(SHOP_PHONE, "Phone");
    }
    public String getShopAddress() {
        return mAppPrefs.prefs.getString(SHOP_ADDRESS, "Address");
    }
    public String getPrinterName() {
        return mAppPrefs.prefs.getString(PRINTER_NAME, "Printer");
    }
    public String getShopGST() {
        return mAppPrefs.prefs.getString(SHOP_GST, "0");
    }
    public String getShopTin() {
        return mAppPrefs.prefs.getString(SHOP_TIN, "tin");
    }


    public void saveSaleCartItems(ArrayList<Product> addedProducts) {
        // Create Gson object.
        Gson gson = new Gson();
        // Get java object list json format string.
        String saleCartJsonString = gson.toJson(addedProducts);
        mAppPrefs.prefs.edit().putString(SALE_SAVE_CART, saleCartJsonString).apply();
    }

    public void clearSaveSaleCartItems() {
        mAppPrefs.prefs.edit().remove(SALE_SAVE_CART).apply();
    }

    public ArrayList<Product> getSaleCartItems() {

        // Get saved string data in it.
        String saleCartItemsJsonString = mAppPrefs.prefs.getString(SALE_SAVE_CART, "");
        // Create Gson object and translate the json string to related java object array.
        Gson gson = new Gson();
        Product productsArray[] = gson.fromJson(saleCartItemsJsonString, Product[].class);

        ArrayList<Product> saleCartItems = new ArrayList<>();

        if(productsArray == null) {
            return saleCartItems;
        }
        // Loop the saleCartItems array

        int length = productsArray.length;
        if(length == 0) { return saleCartItems; }
        for(int i=0;i<length;i++)
        {
            saleCartItems.add(productsArray[i]);
        }
        return saleCartItems;
    }

    public void saveSaleGroupBasketItems(ArrayList<Product> addedProducts, String basketName) {
        // Create Gson object.
        Gson gson = new Gson();
        // Get java object list json format string.
        String saleCartJsonString = gson.toJson(addedProducts);
        mAppPrefs.prefs.edit().putString(basketName, saleCartJsonString).apply();
    }

    public void clearSaleGroupBasketItems(String basketName) {
        mAppPrefs.prefs.edit().remove(basketName).apply();
    }

    public ArrayList<Product> getSaleGroupBasketItems(String basketName) {

        // Get saved string data in it.
        String saleCartItemsJsonString = mAppPrefs.prefs.getString(basketName, "");
        // Create Gson object and translate the json string to related java object array.
        Gson gson = new Gson();
        Product productsArray[] = gson.fromJson(saleCartItemsJsonString, Product[].class);

        ArrayList<Product> saleCartItems = new ArrayList<>();

        if(productsArray == null) {
            return saleCartItems;
        }
        // Loop the saleCartItems array

        int length = productsArray.length;
        if(length == 0) { return saleCartItems; }
        for(int i=0;i<length;i++)
        {
            saleCartItems.add(productsArray[i]);
        }
        return saleCartItems;
    }
}

