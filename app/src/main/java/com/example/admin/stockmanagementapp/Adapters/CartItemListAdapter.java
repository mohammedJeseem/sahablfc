package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.CartItemRemovedEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.BasketManager;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.Utils.Utility;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class CartItemListAdapter extends ArrayAdapter<Product> implements View.OnClickListener {

private ArrayList<Product> dataSet;
        Context mContext;
        Boolean isSaleTypeGroup;



    public CartItemListAdapter(ArrayList<Product> data, Context context, Boolean isSaleTypeGroup) {
        super(context, R.layout.layout_sale_cart_product_card, data);
        this.dataSet = data;
        this.mContext = context;
        this.isSaleTypeGroup = isSaleTypeGroup;


        registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                Log.d("CartListData", "onChanged: ");
            }
        });

    }



    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Product product=(Product) object;

        switch (v.getId())
        {
            case R.id.sale_cart_delete_product:
//                remove(product);
                if (isSaleTypeGroup) {
                    BasketManager.getBasketManager().getCurrentBasket().removeProductFromBasket(product.getProductId());
                } else {
                    CartAddedItem.getCartAddedItem().removeProductFromCart(product.getProductId());
                }


//                notifyDataSetChanged();

                EventBus.getDefault().post(new CartItemRemovedEvent(product.getProductId()));

                Log.d("Vishnu", "onClick: delete btn");
                break;
        }
    }

    private int lastPosition = -1;
    private static int save = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Product product = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

//        convertView.setBackgroundColor(Color.WHITE);
//
//        parent.getChildAt(position).setBackgroundResource(R.color.white);
//
//        if (save != -1 && save != position){
//            parent.getChildAt(save).setBackgroundColor(Color.BLACK);
//        }

//        save = position;
//        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.layout_sale_cart_product_card, parent, false);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.sale_cart_product_img_view);
            viewHolder.name = (TextView) convertView.findViewById(R.id.sale_cart_product_name_tv);
            viewHolder.weight = (TextView) convertView.findViewById(R.id.sale_cart_product_weight_tv);
            viewHolder.quantity = (EditText) convertView.findViewById(R.id.sale_cart_product_qty_count);
            viewHolder.price = (TextView) convertView.findViewById(R.id.sale_cart_product_price_tv);
            viewHolder.deleteBtn = (ImageButton) convertView.findViewById(R.id.sale_cart_delete_product);
            viewHolder.incrementCountBtn = (ImageButton) convertView.findViewById(R.id.sale_cart_increment_count);
            viewHolder.decrementCountBtn = (ImageButton) convertView.findViewById(R.id.sale_cart_decrement_count);

            result=convertView;

            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//            result=convertView;
//        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.name.setText(product.getName());
        viewHolder.weight.setText("in "+product.getUnit());

        float price;
        if (String.valueOf(product.getQuantity()).equals("")) {
            price = 1f;
        } else {
            price = product.getQuantity() * product.getPrice();
        }
        viewHolder.price.setText(Utility.getPriceFormated(price));

        viewHolder.deleteBtn.setTag(position);
        viewHolder.incrementCountBtn.setTag(position);
        viewHolder.decrementCountBtn.setTag(position);
        viewHolder.quantity.setTag(position);
        viewHolder.deleteBtn.setOnClickListener(this);
//        viewHolder.incrementCountBtn.setOnClickListener(this);
//        viewHolder.decrementCountBtn.setOnClickListener(this);

        viewHolder.quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                float price = 0;
                float quantity_value = 0;
                if (!s.toString().equals(".") && !s.toString().equals("") && !s.toString().equals("-1")) {
                    quantity_value = Float.parseFloat(s.toString());
                    price = quantity_value * product.getPrice();
                } else if (s.toString().equals("-1")) {
                    quantity_value = product.getQuantity();
                    price = quantity_value * product.getPrice();
                }

                viewHolder.price.setText(Utility.getPriceFormated(price));

                if (isSaleTypeGroup) {
                    BasketManager.getBasketManager().getCurrentBasket().setBasketItemQuantity(product.getProductId(), quantity_value);
                } else {
                    CartAddedItem.getCartAddedItem().setCartItemQuantity(product.getProductId(), quantity_value);
                }
                EventBus.getDefault().post(new OnCartItemChanged());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        viewHolder.setClickListener();

        if (product.getUnit().equals(AppConstants.Unit.KILOGRAM)) {
            viewHolder.quantity.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            viewHolder.incrementCountBtn.setVisibility(View.GONE);
            viewHolder.decrementCountBtn.setVisibility(View.GONE);
            viewHolder.quantity.setText(String.valueOf(product.getQuantity()));
        } else if (product.getUnit().equals(AppConstants.Unit.NUMBER)) {
            viewHolder.quantity.setInputType(InputType.TYPE_CLASS_NUMBER);
            viewHolder.quantity.setText(String.valueOf((int)product.getQuantity()));
        }

        Glide.with(mContext).load(product.getImage_path())
                .apply(new RequestOptions())
                .into(viewHolder.image);

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView name;
        TextView weight;
        EditText quantity;
        TextView price;
        ImageButton deleteBtn;
        ImageButton incrementCountBtn;
        ImageButton decrementCountBtn;
        ListItemDelegate delegate = null;

        public ViewHolder() {

        }

        public void setClickListener() {
            decrementCountBtn.setOnClickListener(this);
            incrementCountBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.sale_cart_increment_count:
                    quantity.setText(String.valueOf((Integer.parseInt(quantity.getText().toString()) + 1)));
                    break;

                case R.id.sale_cart_decrement_count:
                    int count = Math.max(1, (Integer.parseInt(quantity.getText().toString()) - 1));
                    quantity.setText(String.valueOf(count));
                    break;
            }
        }
    }

    public interface ListItemDelegate {
        void OnItemQuantityChanged(int position);
    }

}
