package com.example.admin.stockmanagementapp.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Models.SaleBillProduct;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;

import java.util.List;

@Dao
public interface DaoAccess {

    //Product

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertProduct(Product product);

    @Update
    int updateProduct(Product product);

    @Delete
    int deleteProduct(Product product);

    @Query("SELECT * FROM Product")
    List<Product> fetchAllProducts();


    // Category

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(Category category);

    @Update
    void updateCategory(Category category);

    @Delete
    void deleteCategory(Category category);

    @Query("SELECT * FROM Category")
    List<Category> fetchAllCategories();


    // Sale

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertSale(Sale sale);

    @Update
    void updateSale(Sale sale);

    @Delete
    void deleteSale(Sale sale);

    @Query("SELECT * FROM Sale")
    List<Sale> fetchAllSales();

    @Insert
    void insertSaleItem(SaleItem saleItem);

    // Get all sale items
    @Query("SELECT Sale.bill_number AS bill_no, Sale.timestamp AS timestamp, Sale.totalPrice AS total_amount, Product.productId AS product_id, Product.name AS product_name, SaleItem.quantity AS quantity, "
            + "Product.unit AS unit, Product.price AS unit_price, Category.name AS category_name, Product.image_path "
            + "FROM Sale "
            + "INNER JOIN SaleItem ON Sale.saleId = SaleItem.saleId "
            + "INNER JOIN Product ON Product.productId = SaleItem.productId "
            + "INNER JOIN Category ON Product.categoryId = Category.categoryId ")
    List<SaleReportInfo> getAllReport();

    // Get sale item in a time range
    @Query("SELECT Sale.bill_number AS bill_no, Sale.timestamp AS timestamp, Sale.totalPrice AS total_amount,Product.productId AS product_id, Product.name AS product_name, SaleItem.quantity AS quantity, "
            + "Product.unit AS unit, Product.price AS unit_price, Category.name AS category_name, Product.image_path "
            + "FROM Sale "
            + "INNER JOIN SaleItem ON Sale.saleId = SaleItem.saleId "
            + "INNER JOIN Product ON Product.productId = SaleItem.productId "
            + "INNER JOIN Category ON Product.categoryId = Category.categoryId "
            + "WHERE Sale.timestamp BETWEEN :startTime AND :endTime")
    List<SaleReportInfo> getReportByTime(long startTime, long endTime);

    // Get sale item by bill no
    @Query("SELECT Product.productId AS productId,Product.categoryId AS categoryId, Product.name AS name, SaleItem.quantity AS quantity, "
            + "Product.unit AS unit, Product.price AS price, Category.name AS category_name "
            + "FROM Sale "
            + "INNER JOIN SaleItem ON Sale.saleId = SaleItem.saleId "
            + "INNER JOIN Product ON Product.productId = SaleItem.productId "
            + "INNER JOIN Category ON Product.categoryId = Category.categoryId "
            + "WHERE Sale.bill_number = :bill_no ")
    List<SaleBillProduct> getProductByBillNo(String bill_no);

    // Delete sale item by bill no
    @Query("DELETE FROM Sale WHERE bill_number = :bill_no")
    int deleteSaleByBillNo (String bill_no);

    // Get Product by keyword
    @Query("SELECT * FROM Product WHERE Product.name LIKE :keyword")
    List<Product> getProductByKey(String keyword);

    // Get Product by category id
    @Query("SELECT * FROM Product WHERE Product.categoryId = :category_id")
    List<Product> getProductByCategoryId(int category_id);

    // Get Product by category id and keyword
    @Query("SELECT * FROM Product WHERE Product.categoryId = :category_id AND Product.name LIKE :keyword")
    List<Product> getProductByIdAndKey(int category_id, String keyword);


    // Basket aka tableGroup
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertBasket(Basket basket);

    @Update
    int updateBasket(Basket basket);

    @Delete
    int deleteBasket(Basket basket);

    @Query("SELECT * FROM Basket")
    List<Basket> fetchAllBaskets();
}
