package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Events.DeleteBasketEvent;
import com.example.admin.stockmanagementapp.Events.GetBasketEvent;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.BasketManager;
import com.example.admin.stockmanagementapp.databinding.LayoutSaleGroupCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class SaleGroupListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private ArrayList<Basket> mSaleGroup = null;
    private Context mContext;

    public SaleGroupListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutSaleGroupCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_sale_group_card, parent, false);

        return new SaleGroupListAdapter.SaleGroupViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final Basket saleGroup = mSaleGroup.get(position);
        final LayoutSaleGroupCardBinding bindingView = ((SaleGroupListAdapter.SaleGroupViewHolder)holder).getLayoutBinding();

        bindingView.saleGroupNameTv.setText(mSaleGroup.get(position).getName());
        int productCount = BasketManager.getBasketManager().getCurrentBasketProductCountById(mSaleGroup.get(position).getBasketId());
        if( productCount > 0) {
            bindingView.groupBadgeCount.setVisibility(View.VISIBLE);
            bindingView.groupBadgeCount.setText(String.valueOf(productCount));
        } else {
            bindingView.groupBadgeCount.setVisibility(View.GONE);
        }

        bindingView.saleGroupDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new DeleteBasketEvent(saleGroup));
            }
        });

        ((SaleGroupListAdapter.SaleGroupViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext,"Item clicked"+position,Toast.LENGTH_LONG).show();
                EventBus.getDefault().post(new GetBasketEvent(mSaleGroup.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mSaleGroup != null)
            return mSaleGroup.size();
        else
            return 0;
    }

    public void setProducts(ArrayList<Basket> mSaleGroup) {
        this.mSaleGroup = mSaleGroup;
        notifyDataSetChanged();
    }

    public class SaleGroupViewHolder extends RecyclerView.ViewHolder{
        private LayoutSaleGroupCardBinding mBinding;

        public SaleGroupViewHolder(LayoutSaleGroupCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutSaleGroupCardBinding getLayoutBinding() {
            return mBinding;
        }
    }
}
