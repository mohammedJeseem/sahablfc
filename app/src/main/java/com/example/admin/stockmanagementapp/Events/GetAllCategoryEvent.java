package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;

import java.util.ArrayList;

public class GetAllCategoryEvent {
    private ArrayList<Category> categories;

    public GetAllCategoryEvent(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
