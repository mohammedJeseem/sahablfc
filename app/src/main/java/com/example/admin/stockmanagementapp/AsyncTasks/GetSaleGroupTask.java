package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Events.GetAllBasketEvent;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class GetSaleGroupTask extends AsyncTask<String, Void, ArrayList<Basket>> {

    @Override
    protected void onPostExecute(ArrayList<Basket> saleGroup) {
        super.onPostExecute(saleGroup);
        if (saleGroup.size() > 0) {
            EventBus.getDefault().post(new GetAllBasketEvent(saleGroup));
        }
    }

    @Override
    protected ArrayList<Basket> doInBackground(String... strings) {

        ArrayList<Basket> saleGroup = (ArrayList<Basket>) AppConstants.getApplicationDB().daoAccess().fetchAllBaskets();
        return saleGroup;
    }
}
