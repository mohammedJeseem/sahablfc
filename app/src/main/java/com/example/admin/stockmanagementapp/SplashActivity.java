package com.example.admin.stockmanagementapp;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Database.ApplicationDB;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.AppPrefs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;

public class SplashActivity extends AppCompatActivity {

    private static String KEY_VALUE = "some_key"; //hardcode secret key here
    private static String KEY_FILENAME = "key.txt"; //file contain key
    private static String DB_NAME = "database-v0.1"; //database name
    private File KEY_DIR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();

//        test();
    }
    private void init(){
        KEY_DIR = getExternalFilesDir(null);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                authenticate();
                test();
            }
        },3000L);
    }

    private void test() {
        initDB();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        Intent intent = new Intent(getApplicationContext(), ListProductActivity.class);
        startActivity(intent);
        finish();
    }

    private void authenticate() {
        if (!KEY_DIR.exists()) {
            KEY_DIR.mkdir();
        }
        File key_file = new File(KEY_DIR, KEY_FILENAME);
        if (key_file.exists()) {
            //Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(key_file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                }
                br.close();

            }
            catch (IOException e) {
                handelFailure();
            }

            if (text.toString().equals(KEY_VALUE)) {
                initDB();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                handelFailure();
            }

        } else {
            handelFailure();
        }
    }

    private void handelFailure() {
        Toast.makeText(this, "Authentication Failed !", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void initDB() {
        AppConstants.setApplicationDB(Room.databaseBuilder(getApplicationContext(), ApplicationDB.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
                .build());

        AppPrefs prefs = AppPrefs.getInstance(SplashActivity.this);
        if(prefs.getLastBillNo() == null || prefs.getLastBillNo() == 0000l) {
            prefs.iniBillNo();
        }

        if(prefs.getLastCounterNo() == null || prefs.getLastCounterNo() == 0l) {
            prefs.iniCounterNo();
        }

        if(prefs.getLastDateNo() == null || prefs.getLastDateNo() == 0l) {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());

//getting the desired properties
            cal.get(Calendar.YEAR);
            cal.get(Calendar.MONTH);
            cal.get(Calendar.DAY_OF_MONTH);

            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 0);


            long timeStampForEndDay = cal.getTimeInMillis() / 1000;



            prefs.iniLastDateNo(timeStampForEndDay);
        }

    }

}
