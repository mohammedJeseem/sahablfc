package com.example.admin.stockmanagementapp.Utils;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Product;

import java.util.ArrayList;

public class BasketManager {

    private static BasketManager mBasketManager;
    private ArrayList<Basket> mBaskets;
    private int mcurrentBasketIndex;


    public static BasketManager getBasketManager () {

        if (mBasketManager == null) {
            mBasketManager = new BasketManager();
        }

        return mBasketManager;
    }

    public ArrayList<Basket> getAllBaskets() {
        return mBaskets;
    }

    public void setBaskets(ArrayList<Basket> mBaskets) {

        if (this.mBaskets != null && this.mBaskets.size() > 0) {
                return;
        }

        this.mBaskets = mBaskets;
        mcurrentBasketIndex = 0;
    }

    public Basket getCurrentBasket() {

        if (mcurrentBasketIndex >= 0 && mBaskets.size() > 0) {
            return mBaskets.get(mcurrentBasketIndex);
        }

        return null;
    }

    public int getAllBasketCount () {
        if (mBaskets != null && mBaskets.size() > 0) {
            return mBaskets.size();
        } else {
            return 0;
        }
    }

    public int getCurrentBasketCount () {
        if (mBaskets != null && mBaskets.get(mcurrentBasketIndex) != null && mBaskets.get(mcurrentBasketIndex).getProductCount() > 0) {
            return mBaskets.get(mcurrentBasketIndex).getProductCount();
        } else {
            return 0;
        }
    }

    public void addProductToCurrentBasket (Product product) {

        getCurrentBasket().addProductToCart(product);
    }


    public void setCurrentBasketIndexById (int id) {

        for (int i = 0; i < mBaskets.size(); i++ ) {
            if (mBaskets.get(i).getBasketId() == id) {
                mcurrentBasketIndex = i;
                break;
            }
        }
    }

    public void clearBasketById (int id) {

        for (int i = 0; i < mBaskets.size(); i++ ) {
            if (mBaskets.get(i).getBasketId() == id) {

                mBaskets.get(i).clearBasketItem();
                break;
            }
        }
    }

    public void addBasket (Basket basket) {

        if (mBaskets == null) {
            mBaskets = new ArrayList<>();
        }
        mBaskets.add(basket);
    }

    public int getCurrentBasketProductCountById (int id) {

        int count = 0;
        for (int i = 0; i < mBaskets.size(); i++ ) {
            if (mBaskets.get(i).getBasketId() == id) {
                count = mBaskets.get(i).getProductCount();
                break;
            }
        }
        return count;
    }
}
