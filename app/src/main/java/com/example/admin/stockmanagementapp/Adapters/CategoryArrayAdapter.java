package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.R;

import java.util.ArrayList;
import java.util.List;

public class CategoryArrayAdapter extends ArrayAdapter<Category> {

    private Context mContext;
    private List<Category> categoryList = new ArrayList<>();
    private int mResource;
    private LayoutInflater mInflater;

    public CategoryArrayAdapter(@NonNull Context context, List<Category> categoryList) {
        super(context, 0, categoryList);
        this.mContext = context;
        this.categoryList = categoryList;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getView(position, convertView, parent);
        View listItem = convertView;
        if(listItem == null)
//            listItem = LayoutInflater.from(mContext).inflate(R.layout.layout_category_item,parent,false);
            listItem = LayoutInflater.from(mContext).inflate(R.layout.custom_category_spinner_item,parent,false);

        Category currentCategory = categoryList.get(position);

        CheckedTextView category = (CheckedTextView) listItem.findViewById(R.id.category_list_tv);
        category.setText(currentCategory.getName());

        return listItem;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Log.d("Vishnu", "getDropDownView: "+position);
//        return super.getDropDownView(position, convertView, parent);


        if(convertView == null){
//            convertView = mInflater.inflate(R.layout.layout_category_item,parent, false);
            convertView = mInflater.inflate(R.layout.custom_category_spinner_item,parent, false);
        }
        Category category = getItem(position);
        TextView name = (TextView) convertView.findViewById(R.id.category_list_tv);
        name.setText(category.getName());

        return convertView;
    }



    //    @Override
//    public View getDropDownView(int position, @Nullable View convertView,
//                                @NonNull ViewGroup parent) {
////        return createItemView(position, convertView, parent);
//
//        final View view = mInflater.inflate(mResource, parent, false);
//        Category currentCategory = categoryList.get(position);
//        TextView category = (TextView) view.findViewById(R.id.category_list_tv);
//        category.setText(currentCategory.getName());
//
//        return view;
//    }


    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

//        TextView offTypeTv = (TextView) view.findViewById(R.id.category_list_tv);
//
//        Offer offerData = items.get(position);
//
//        offTypeTv.setText(offerData.getOfferType());
//        numOffersTv.setText(offerData.getNumberOfCoupons());
//        maxDiscTV.setText(offerData.getMaxDicount());



        return view;
    }
}
