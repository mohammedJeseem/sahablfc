package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.databinding.LayoutCategoryCardBinding;
import com.example.admin.stockmanagementapp.databinding.LayoutProductCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ListCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Category> mCategories = null;
    private Context mContext;

    public ListCategoryAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutCategoryCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_category_card, parent, false);

        return new CategoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final Category category = mCategories.get(position);
        LayoutCategoryCardBinding bindingView = ((CategoryViewHolder)holder).getLayoutBinding();

        bindingView.categoryNameTv.setText(mCategories.get(position).getName());
//        Glide.with(mContext).load(category.getImage_path())
//                .apply(new RequestOptions())
//                .into(bindingView.categoryImgView);

    }

    @Override
    public int getItemCount() {
        if (mCategories != null)
            return mCategories.size();
        else
            return 0;
    }

    public void setProducts(ArrayList<Category> mCategories) {
        this.mCategories = mCategories;
        notifyDataSetChanged();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        private LayoutCategoryCardBinding mBinding;

        public CategoryViewHolder(LayoutCategoryCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutCategoryCardBinding getLayoutBinding() {
            return mBinding;
        }

    }
}
