package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class GetCategoryTask extends AsyncTask<String, Void, ArrayList<Category>> {
    @Override
    protected void onPostExecute(ArrayList<Category> categories) {
        super.onPostExecute(categories);
        if (categories.size() > 0) {
            EventBus.getDefault().post(new GetAllCategoryEvent(categories));
        }
    }

    @Override
    protected ArrayList<Category> doInBackground(String... strings) {

        ArrayList<Category> categories = (ArrayList<Category>) AppConstants.getApplicationDB().daoAccess().fetchAllCategories();
        return categories;
    }
}
