package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.DBChangeEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

public class DeleteProductTask extends AsyncTask<Product, Void, Boolean> {
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        EventBus.getDefault().post(new DBChangeEvent(aBoolean,DBChangeEvent.PRODUCT_DELETE
        ));
    }

    @Override
    protected Boolean doInBackground(Product... products) {
        int id = AppConstants.getApplicationDB().daoAccess().deleteProduct(products[0]);
        if (id > 0) {
            return true;
        }
        return false;
    }
}
