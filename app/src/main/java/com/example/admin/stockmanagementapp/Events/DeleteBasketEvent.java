package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Basket;

import java.util.ArrayList;

public class DeleteBasketEvent {

    private Basket saleGroup;

    public DeleteBasketEvent(Basket saleGroup) {
        this.saleGroup = saleGroup;
    }

    public Basket getBasketForDelete() {
        return saleGroup;
    }

}
