package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;

import java.util.ArrayList;

public class GetAllBasketEvent {

    private ArrayList<Basket> saleGroup;

    public GetAllBasketEvent(ArrayList<Basket> saleGroup) {
        this.saleGroup = saleGroup;
    }

    public ArrayList<Basket> getBaskets() {
        return saleGroup;
    }
}
