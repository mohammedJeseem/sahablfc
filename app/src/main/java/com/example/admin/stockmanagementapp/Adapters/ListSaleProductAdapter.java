package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.OnProductAddedCartEvent;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.LayoutSaleCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ListSaleProductAdapter extends RecyclerView.Adapter<ListSaleProductAdapter.ProductViewHolder> {
    private ArrayList<Product> mProducts = null;
    private Context mContext;

    public ListSaleProductAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ListSaleProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutSaleCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_sale_card, parent, false);

        return new ListSaleProductAdapter.ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ListSaleProductAdapter.ProductViewHolder holder, int position) {
        final Product product = mProducts.get(position);
        LayoutSaleCardBinding bindingView = ((ListSaleProductAdapter.ProductViewHolder)holder).getLayoutBinding();

        // bindingView.productCategoryTv.setText(mProducts.get(position).getCategoryId());
        bindingView.saleNameTv.setText(mProducts.get(position).getName());
        Glide.with(mContext).load(product.getImage_path())
                .apply(new RequestOptions())
                .into(bindingView.saleImgView);
//        final String price = product.getPrice().toString() + " Rs";
        bindingView.salePriceTv.setText(Utility.getPriceFormated(product.getPrice()));
        bindingView.saleCategoryTv.setText(product.getCategory_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EventBus.getDefault().post(new OnProductAddedCartEvent(product));
            }
        });
    }


    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    public void setProducts(ArrayList<Product> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{
        private LayoutSaleCardBinding mBinding;

        public ProductViewHolder(LayoutSaleCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutSaleCardBinding getLayoutBinding() {
            return mBinding;
        }

    }
}
