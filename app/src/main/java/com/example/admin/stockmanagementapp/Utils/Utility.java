package com.example.admin.stockmanagementapp.Utils;

import android.content.Context;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class Utility {

    public static float pxToDp(final Context context, final float px) {

        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float dpToPx(final Context context, final float dp) {

        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static String getPriceFormated(Float price) {
        Locale locale = new Locale("en", "in");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        format.setCurrency(Currency.getInstance(locale));
        return format.format(price);
    }
}
