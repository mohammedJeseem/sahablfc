package com.example.admin.stockmanagementapp.Entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity
public class Sale {

    @PrimaryKey(autoGenerate = true)
    private int saleId;
    public String bill_number;
    private float totalPrice;
    private long timestamp;

    public Sale(String bill_number, float totalPrice, long timestamp) {
        this.bill_number = bill_number;
        this.totalPrice = totalPrice;
        this.timestamp = timestamp;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getBill_number() {
        return bill_number;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getTotalPrice() {
        return totalPrice;
    }
}
