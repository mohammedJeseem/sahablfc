package com.example.admin.stockmanagementapp.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.GetAllReportEvent;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class GetReportTask extends AsyncTask<Long, Void, List<SaleReportInfo>>{
    Context mContext;

    public GetReportTask(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    protected void onPostExecute(List<SaleReportInfo> report) {
        super.onPostExecute(report);
        EventBus.getDefault().post(new GetAllReportEvent(report));
    }

    @Override
    protected List<SaleReportInfo> doInBackground(Long... date) {
        List<SaleReportInfo> list = AppConstants.getApplicationDB().daoAccess().getReportByTime(date[0], date[1]);
        return list;
    }

}
