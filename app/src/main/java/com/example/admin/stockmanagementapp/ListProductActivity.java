package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.CategoryArrayAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListProductAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetProductsTask;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.GetAllProductEvent;
import com.example.admin.stockmanagementapp.Events.GetProductEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.Events.RefreshListEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.databinding.ActivityListProductBinding;
import com.mancj.materialsearchbar.MaterialSearchBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class ListProductActivity extends AppCompatActivity {

    ActivityListProductBinding mBinding;
    ListProductAdapter mAdapter;
    ArrayList<Product> mProducts = new ArrayList<>();
    MaterialSearchBar materialSearchBar;
    List<String> suggestList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_list_product);
        mAdapter = new ListProductAdapter(this);
        mBinding.listProduct.setAdapter(mAdapter);
        mBinding.listProduct.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        suggestList = new ArrayList<>();
        getProducts(-1,"");
        setRefreshLayout();
        setupSearchBar();
        populateCategoryMenu();

        Toolbar myToolbar = mBinding.listProductToolbar;
//        //setSupportActionBar(myToolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        myToolbar.setTitle("List Products");
        setSupportActionBar(myToolbar);

        setAddProductFAB();

    }

    private void setRefreshLayout() {
        mBinding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProducts(-1,"");
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//            case R.id.action_add: // Product Add btn on top-right
//                Intent intent = new Intent(getApplicationContext(),ProductActivity.class);
//                intent.putExtra("add_product", true);
//                startActivity(intent);
//                break;
//
//            default:
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void setAddProductFAB() {
        mBinding.addProductFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ProductActivity.class);
                intent.putExtra("add_product", true);
                startActivity(intent);
            }
        });
    }

    private void getProducts(int category_id, String keyword) {
        GetProductsTask task = new GetProductsTask(this);
        task.execute(new GetProductsTask.Params(category_id, keyword));


//       for(int i = 1; i <= 10; i++) {
//           Product testProduct = new Product(i, "test product"+i, AppConstants.Unit.KILOGRAM, "", 100.0f);
//           mProducts.add(testProduct);
//        }
//        mAdapter.setProducts(mProducts);

    }

    private void populateCategoryMenu() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }

    private void setupSearchBar () {

        materialSearchBar = findViewById(R.id.search_bar);
        materialSearchBar.hideSuggestionsList();
//        materialSearchBar.setLastSuggestions(suggestList);
//        materialSearchBar.setMaxSuggestionCount(4);
        materialSearchBar.setCardViewElevation(10);
        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                List<String> suggest = new ArrayList<String>();
                for(String search : suggestList){
                    if(search.toLowerCase().contains(materialSearchBar.getText().toLowerCase())){
                        suggest.add(search);
                    }
//                    materialSearchBar.setLastSuggestions(suggest);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                String keyword = "%" + s.toString().toLowerCase() + "%";
                getProducts(((Category) mBinding.categorySpinner.getSelectedItem()).getCategoryId(),keyword);
            }
        });

        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

                if(!enabled){
                    mBinding.listProduct.setAdapter(mAdapter);
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {

                startSearch(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    private void startSearch(CharSequence text) {

    }

    private void setCategorySpinner(ArrayList<Category> categories) {


        ArrayList<Category> categoriesCopy = new ArrayList<Category>(categories);

        Category emptyCategoryItem = new Category("--category--");
        emptyCategoryItem.setCategoryId(-1);
        categoriesCopy.add(0, emptyCategoryItem);

        ArrayAdapter<Category> adapter =
                new CategoryArrayAdapter(this, categoriesCopy);

        mBinding.categorySpinner.setAdapter(adapter);
        //mBinding.saleCategorySpinner.setSelection(getCategorySelectPosition(categories));

        mBinding.categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int category_id = ((Category)mBinding.categorySpinner.getSelectedItem()).getCategoryId();
                getProducts(category_id, mBinding.searchBar.getText().toLowerCase());
                Log.d("Vishnu", "onItemSelected: "+((Category)mBinding.categorySpinner.getSelectedItem()).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onListItemClickedEvent(GetProductEvent event) {
        Intent intent = new Intent(this, ProductActivity.class);
        intent.putExtra("product", event.getProduct());
        startActivity(intent);
    }

    @Subscribe
    public void onGetProductEvent(GetAllProductEvent event) {
        if (event.getProducts().size() < 1) {
            mBinding.msgNoProduct.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Products", Toast.LENGTH_LONG).show();
        } else {
            mBinding.msgNoProduct.setVisibility(View.GONE);
        }
        mBinding.refreshLayout.setRefreshing(false);
        mAdapter.setProducts(event.getProducts());
    }

    @Subscribe(sticky = true)
    public void onRefreshEvent(RefreshListEvent event) {
        if (event.getType() == RefreshListEvent.REFRESH_PRODUCT) {
            getProducts(-1,"");
        }
    }

    @Subscribe
    public void onCategoryLoaded(GetAllCategoryEvent event) {
        setCategorySpinner(event.getCategories());

        for (Category s:event.getCategories()) {
            Log.d("List", "onCategoryLoaded: "+s.getName());
        }
    }

}
