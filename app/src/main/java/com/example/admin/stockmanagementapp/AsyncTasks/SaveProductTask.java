package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.DBChangeEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

public class SaveProductTask extends AsyncTask<Product, Void, Boolean> {

    boolean isUpdate;

    public SaveProductTask(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(isUpdate)
            EventBus.getDefault().post(new DBChangeEvent(aBoolean, DBChangeEvent.PRODUCT_UPDATE));
        else
            EventBus.getDefault().post(new DBChangeEvent(aBoolean, DBChangeEvent.PRODUCT_INSERT));
    }

    @Override
    protected Boolean doInBackground(Product... products) {
        if (isUpdate) {
            int id = 0;
            id = AppConstants.getApplicationDB().daoAccess().updateProduct(products[0]);
            if (id > 0) return  true;
        } else {
            Long id = AppConstants.getApplicationDB().daoAccess().insertProduct(products[0]);
            if(id != null) return true;
        }

        return false;
    }
}

