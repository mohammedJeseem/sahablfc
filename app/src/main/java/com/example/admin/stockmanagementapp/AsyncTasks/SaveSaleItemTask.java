package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Events.OnSaleSavedEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class SaveSaleItemTask extends AsyncTask<SaveSaleItemTask.Params, Void, Boolean> {

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        EventBus.getDefault().post(new OnSaleSavedEvent(aBoolean));
    }

    @Override
    protected Boolean doInBackground(Params... params) {
        long saleId = AppConstants.getApplicationDB().daoAccess().insertSale(params[0].sale);
        for (int i = 0; i < params[0].saleItems.size(); i++) {
            params[0].saleItems.get(i).setSaleId(saleId);
            AppConstants.getApplicationDB().daoAccess().insertSaleItem(params[0].saleItems.get(i));
        }
        return true;
    }

    public static class Params {
        private Sale sale;
        private ArrayList<SaleItem> saleItems;

        public Params(Sale sale, ArrayList<SaleItem> saleItems) {
            this.sale = sale;
            this.saleItems = saleItems;
        }
    }
}
