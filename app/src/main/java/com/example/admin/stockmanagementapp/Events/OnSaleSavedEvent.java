package com.example.admin.stockmanagementapp.Events;

public class OnSaleSavedEvent {
    private boolean success;

    public OnSaleSavedEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
