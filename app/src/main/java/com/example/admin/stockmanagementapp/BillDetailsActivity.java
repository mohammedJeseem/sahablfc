package com.example.admin.stockmanagementapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.ListBillDetailsPreviewAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListCartPreviewAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetBillDetailsTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetReportTask;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetAllReportEvent;
import com.example.admin.stockmanagementapp.Events.GetBillProductsEvents;
import com.example.admin.stockmanagementapp.Models.SaleBillProduct;
import com.example.admin.stockmanagementapp.Utils.AppPrefs;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.ActivityBillDetailsBinding;
import com.example.admin.stockmanagementapp.databinding.ActivityReportBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class BillDetailsActivity extends AppCompatActivity {

    ActivityBillDetailsBinding mBinding;
    private static final int MENU_PRINT_ID = 501;
    String selectedBillNo;
    ArrayList<SaleBillProduct> billProducts;
    ListBillDetailsPreviewAdapter mPreviewAdapter;

    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;

    OutputStream outputStreamPrinter = null;
    InputStream inputStream;
    Thread thread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    float finalAmnt = 0;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_details);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bill_details);

        mPreviewAdapter = new ListBillDetailsPreviewAdapter();

        if (getIntent().getSerializableExtra("billNo") != null) {

            selectedBillNo = (String) getIntent().getSerializableExtra("billNo");
//            Toast.makeText(this, "Bill : "+selectedBillNo, Toast.LENGTH_LONG).show();

            getProductByBillNo(selectedBillNo);
        }

        mBinding.backButtonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getProductByBillNo(String selectedBillNo) {

        GetBillDetailsTask task = new GetBillDetailsTask(BillDetailsActivity.this);
        task.execute(selectedBillNo);
    }


    public void showPopupReport(View v) {
        PopupMenu popup = new PopupMenu(BillDetailsActivity.this, v);
        popup.getMenu().add(1, MENU_PRINT_ID, 1, "Print Bill");
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case MENU_PRINT_ID:
                        Toast.makeText(BillDetailsActivity.this, "ClickedPrint", Toast.LENGTH_LONG).show();

                        printBill();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void showProgressBar(boolean show) {
        if (show) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ReportActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (outputStreamPrinter != null) {
            try {
                outputStreamPrinter.close();
                outputStreamPrinter = null;
            } catch (IOException ex){

            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
                inputStream = null;
            } catch (IOException ex){

            }
        }
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
                bluetoothSocket = null;
            } catch (IOException ex){

            }
        }

        stopWorker=true;
    }

    @Subscribe
    public void onGetBillDetailProductsEvent(GetBillProductsEvents event) {


        billProducts = (ArrayList<SaleBillProduct>) event.getProducts();
        previewPayment();
//        Toast.makeText(this,"Length" + event.getProducts().size(),Toast.LENGTH_LONG).show();
    }


    private void previewPayment() {


        if(billProducts == null || billProducts.size() < 1) return;

        mBinding.billDetailsBillNo.setText("Bill No : "+selectedBillNo);

        mPreviewAdapter.setProducts(billProducts);
        mBinding.billDetailsPreviewList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mBinding.billDetailsPreviewList.setAdapter(mPreviewAdapter);



        for(int i = 0; i < billProducts.size(); i++){

            float amount = billProducts.get(i).getQuantity() * billProducts.get(i).getPrice();
            finalAmnt = finalAmnt + amount;
        }
        
        mBinding.billDetailsTotalAmountTv.setText(Utility.getPriceFormated(finalAmnt));
    }


    private void printBill() {
        showProgressBar(true);

        if(outputStreamPrinter != null) {

            printData();
        } else {
            connectPrinter();
        }
    }



    private void connectPrinter() {
        try{
            findBluetoothDevice();
//            openBluetoothPrinter();
//            printData();

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }
    }

    void findBluetoothDevice(){

        try{

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(bluetoothAdapter==null){
                handlePrinterError("Bluetooth is disabled");
            }
            if(!bluetoothAdapter.isEnabled()){
//                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBT,0);

                handlePrinterError("Bluetooth is disabled");
            }

            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();

            if(pairedDevice.size()>0){
                for(BluetoothDevice pairedDev:pairedDevice){

                    // My Bluetoth printer name is BTP_F09F1A
                    if(pairedDev.getName().equals(AppPrefs.getInstance(this).getPrinterName())){
                        bluetoothDevice=pairedDev;

                        openBluetoothPrinter();
                        break;
                    }
                }
            } else {

                handlePrinterError("No paired device found");
            }

        }catch(Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }

    }

    // Open Bluetooth Printer

    void openBluetoothPrinter() throws IOException {
        try{

            //Standard uuid from string //
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStreamPrinter=bluetoothSocket.getOutputStream();

            inputStream=bluetoothSocket.getInputStream();
            beginListenData();


            if(outputStreamPrinter != null) {
                printData();
            } else {
                handlePrinterError("Couldn't connect printer");
            }

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }
    }
    void beginListenData(){
        try{

            final Handler handler =new Handler();
            final byte delimiter=10;
            stopWorker =false;
            readBufferPosition=0;
            readBuffer = new byte[1024];

            thread=new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimiter){
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer,0,
                                                encodedByte,0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte,"US-ASCII");
                                        readBufferPosition=0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Log.d("beginListenData", data);
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                    }
                                }
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                        }
                    }

                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void handlePrinterError(String toastMsg) {

        showProgressBar(false);
        alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("No Printer attached");

        alertDialog.setMessage("Please connect printer and retry.");

        alertDialog.setCancelable(true);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Retry", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                connectPrinter();

            } });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

            }});

        alertDialog.show();
        Toast.makeText(BillDetailsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
    }


    void printData(){

        try{
            outputStreamPrinter.flush();
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };

//            // Bold
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nbold".getBytes());
//
//            // Height
//            format[2] = ((byte)(0x10 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nheight".getBytes());
//
//            // Width
//            format[2] = ((byte) (0x20 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nwidth".getBytes());
//
//            // Underline
//            format[2] = ((byte)(0x80 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nUnderline".getBytes());
//
//            // Small
//            format[2] = ((byte)(0x1 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nsmall".getBytes());



            byte[] PRINT_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
            byte[] PRINT_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
            byte[] PRINT_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };

            outputStreamPrinter.write(PRINT_ALIGN_CENTER);
            String companyDetails = "\n" + AppPrefs.getInstance(this).getShopName() + "\n";
            outputStreamPrinter.write(companyDetails.getBytes());
            outputStreamPrinter.write(PRINT_ALIGN_CENTER);
            String placeDetails = AppPrefs.getInstance(this).getShopAddress() + "\n";
            outputStreamPrinter.write(placeDetails.getBytes());
            outputStreamPrinter.write(PRINT_ALIGN_CENTER);
            String phoneDetails = AppPrefs.getInstance(this).getShopPhone() + "\n";
            outputStreamPrinter.write(phoneDetails.getBytes());
            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
            String gstTin = AppPrefs.getInstance(this).getShopTin();
            outputStreamPrinter.write(gstTin.getBytes());


            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String dateAndTime = "\n"+formatter.format(date);
            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
            outputStreamPrinter.write(dateAndTime.getBytes());

            String bill_num = "\nBill NO : "+selectedBillNo+"\n";
            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
            outputStreamPrinter.write(bill_num.getBytes());

            //String line_seperator = "\n----------------------------\n";
            String line_seperator = "-----------------------";
            //outputStreamPrinter.write(line_seperator.getBytes());
            String header = line_seperator+"\nItem \t Rate \t Qty \t Amnt \n"+line_seperator;
            outputStreamPrinter.write(header.getBytes());
            //outputStreamPrinter.write(line_seperator.getBytes());
            outputStreamPrinter.write(PRINT_ALIGN_RIGHT);
            for(int i = 0; i < billProducts.size(); i++){
                String name = billProducts.get(i).getName();
                String rate = String.valueOf(billProducts.get(i).getPrice());
                String quantity = String.valueOf(billProducts.get(i).getQuantity());
                String amount = String.format("%.2f", (billProducts.get(i).getPrice() * billProducts.get(i).getQuantity()));
                String  data = "\n"+name+"\n";

                String  dummy = "";
                String  data1 = dummy+"\t  "+rate+"\t  "+quantity+"\t  "+amount+"\n";
                /*if(name.length() > 6){
                    data = "\n"+name+"\n"+"\t"+rate+"\t "+quantity+"\t "+amount+"\n";
                }else{
                    data = "\n"+name+"\t "+rate+"\t "+quantity+"\t "+amount+"\n";
                }*/
                outputStreamPrinter.write(PRINT_ALIGN_LEFT);
                outputStreamPrinter.write(data.getBytes());
                outputStreamPrinter.write(PRINT_ALIGN_RIGHT);
                outputStreamPrinter.write(data1.getBytes());
            }

//            String net_tot = line_seperator+"\nTOT AMNT : Rs " + String.format("%.2f", finalAmnt) +"\n";
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStreamPrinter.write(format);
//            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
//            outputStreamPrinter.write(net_tot.getBytes());

//            String discount_price = "DISC "+Float.toString(mDiscount)+"% : Rs " + String.format("%.2f", discPrice) +"\n";
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStreamPrinter.write(format);
//            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
//            outputStreamPrinter.write(discount_price.getBytes());
//
//            String net_price = "NET AMNT : Rs " + String.format("%.2f", netPrice) +"\n";
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStreamPrinter.write(format);
//            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
//            outputStreamPrinter.write(net_price.getBytes());
//
//            String gst_price = "GST "+ AppPrefs.getInstance(this).getShopGST()+"% : Rs " + String.format("%.2f", gstPrice) +"\n"+line_seperator+"\n";
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStreamPrinter.write(format);
//            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
//            outputStreamPrinter.write(gst_price.getBytes());

            String total_price = line_seperator+"\nTOTAL : Rs " + String.format("%.2f", finalAmnt) +"\n"+line_seperator+"\n";
            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStreamPrinter.write(format);
            outputStreamPrinter.write(PRINT_ALIGN_LEFT);
            outputStreamPrinter.write(total_price.getBytes());


            outputStreamPrinter.write(PRINT_ALIGN_CENTER);
            String msg = " *** THANK YOU ***\n";
            outputStreamPrinter.write(msg.getBytes());
            String msg1 = "VISIT AGAIN\n\n\n";
            outputStreamPrinter.write(PRINT_ALIGN_CENTER);
            outputStreamPrinter.write(msg1.getBytes());


            outputStreamPrinter.flush();

            showProgressBar(false);

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't print");
        }
    }

    private String getPrintData() {
        String data;
        data = "\nCompany Name\n" +
                "";

        return data;
    }


}
