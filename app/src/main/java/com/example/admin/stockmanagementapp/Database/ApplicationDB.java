package com.example.admin.stockmanagementapp.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Interfaces.DaoAccess;

@Database(entities = {Product.class, Category.class, Sale.class, SaleItem.class, Basket.class}, version = 1, exportSchema = false)
public abstract class ApplicationDB extends RoomDatabase {
    public abstract DaoAccess daoAccess();
}
