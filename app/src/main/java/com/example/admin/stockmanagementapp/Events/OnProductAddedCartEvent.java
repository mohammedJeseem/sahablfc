package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Product;

public class OnProductAddedCartEvent {
    private Product mProduct;

    public OnProductAddedCartEvent(Product mProduct) {
        this.mProduct = mProduct;
    }

    public Product getProduct() {
        return mProduct;
    }
}
