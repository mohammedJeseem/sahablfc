package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Events.GetBillDetailsEvent;
import com.example.admin.stockmanagementapp.Events.GetProductEvent;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.databinding.LayoutReportBillwiseCardBinding;
import com.example.admin.stockmanagementapp.databinding.LayoutReportProductCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ListReportBillwiseAdapter extends RecyclerView.Adapter<ListReportBillwiseAdapter.ReportBillWiseViewHolder>{

    private List<SaleReportInfo> mProducts = null;
    private Context mContext;

    public ListReportBillwiseAdapter(Context mContext) {
        this.mContext = mContext;
    }



    @NonNull
    @Override
    public ListReportBillwiseAdapter.ReportBillWiseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutReportBillwiseCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.layout_report_billwise_card, viewGroup, false);

        return new ListReportBillwiseAdapter.ReportBillWiseViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportBillWiseViewHolder holder, final int position) {

        final SaleReportInfo product = mProducts.get(position);
        LayoutReportBillwiseCardBinding bindingView = holder.getLayoutBinding();

        bindingView.reportBillNumberTv.setText(mProducts.get(position).getBill_no());

        String price = product.getTotal_amount() + " Rs";
        bindingView.reportBillAmountTv.setText(price);

        long timeStamp = mProducts.get(position).getTimestamp() * 1000;
        DateFormat timeFormat = new SimpleDateFormat("dd/MM/YYYY(HH:mm:ss)");
        Date d = new Date(timeStamp);

        bindingView.reportBillDateTv.setText(timeFormat.format(d));

        ((ListReportBillwiseAdapter.ReportBillWiseViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new GetBillDetailsEvent(mProducts.get(position).getBill_no(), false));
            }
        });


        ((ListReportBillwiseAdapter.ReportBillWiseViewHolder) holder).itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Log.d("Vishnu", "onLongClick: ");
                EventBus.getDefault().post(new GetBillDetailsEvent(mProducts.get(position).getBill_no(), true));
                return true;
            }
        });


    }

    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    public void setBillWiseProducts(List<SaleReportInfo> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class ReportBillWiseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private LayoutReportBillwiseCardBinding mBinding;

        public ReportBillWiseViewHolder(LayoutReportBillwiseCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutReportBillwiseCardBinding getLayoutBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {

        }
    }

}
