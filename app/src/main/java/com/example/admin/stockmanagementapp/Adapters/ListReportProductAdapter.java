package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetProductEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.LayoutProductCardBinding;
import com.example.admin.stockmanagementapp.databinding.LayoutReportProductCardBinding;
import com.example.admin.stockmanagementapp.databinding.LayoutSaleCartProductCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ListReportProductAdapter extends RecyclerView.Adapter<ListReportProductAdapter.ReportProductViewHolder> {

    private List<SaleReportInfo> mProducts = null;
    private Context mContext;

    public ListReportProductAdapter(Context mContext) {
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ListReportProductAdapter.ReportProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutReportProductCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_report_product_card, parent, false);

        return new ListReportProductAdapter.ReportProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportProductViewHolder holder, int position) {
        final SaleReportInfo product = mProducts.get(position);
        LayoutReportProductCardBinding bindingView = holder.getLayoutBinding();


        // bindingView.productCategoryTv.setText(mProducts.get(position).getCategoryId());
        bindingView.reportProductNameTv.setText(mProducts.get(position).getProduct_name());
        Glide.with(mContext).load(product.getImage_path())
                .apply(new RequestOptions())
                .into(bindingView.reportProductImgView);
        String price = product.getTotal_amount() + " Rs";
        bindingView.reportProductPriceTv.setText(price);
        String quantity = String.valueOf(product.getQuantity()+" "+product.getUnit());
        bindingView.reportProductWeightTv.setText(quantity);

        ((ListReportProductAdapter.ReportProductViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //EventBus.getDefault().post(new GetProductEvent(product));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    public void setProducts(List<SaleReportInfo> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class ReportProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private LayoutReportProductCardBinding mBinding;

        public ReportProductViewHolder(LayoutReportProductCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutReportProductCardBinding getLayoutBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {

        }
    }
}
