package com.example.admin.stockmanagementapp.Events;

public class DBChangeEvent {
    public static int PRODUCT_INSERT = 0;
    public static int PRODUCT_UPDATE = 1;
    public static int PRODUCT_DELETE = 2;
    public static int CATEGORY_INSERT = 3;
    public static int SALE_INSERT = 4;
    public static int SALE_DELETE = 5;
    boolean isSuccess;
    int type;

    public DBChangeEvent(boolean isSuccess, int type) {
        this.isSuccess = isSuccess;
        this.type = type;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public int getType() {
        return type;
    }
}
