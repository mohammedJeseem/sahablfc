package com.example.admin.stockmanagementapp.Utils;

import com.example.admin.stockmanagementapp.Database.ApplicationDB;

public class AppConstants {

    private static ApplicationDB applicationDB;
    private static String DB_NAME = "database-v0.1";

    public static String SALE_TYPE = "sale_type";
    public static String SALE_TYPE_DEFAULT = "sale_type_default";
    public static String SALE_TYPE_GROUP = "sale_type_group";


    public static ApplicationDB getApplicationDB() {
        return applicationDB;
    }

    public static void setApplicationDB(ApplicationDB applicationDB) {
        AppConstants.applicationDB = applicationDB;
    }

    public static String getDBName() {
        return DB_NAME;
    }

    public final class Unit {
        public static final String KILOGRAM = "Kg";
        public static final String NUMBER = "Nos";
    }
}
