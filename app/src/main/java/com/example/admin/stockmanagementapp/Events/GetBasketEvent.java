package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Basket;

public class GetBasketEvent {

    private Basket basket;

    public GetBasketEvent(Basket basket) {
        this.basket = basket;
    }

    public Basket getBasket() {
        return basket;
    }

}
