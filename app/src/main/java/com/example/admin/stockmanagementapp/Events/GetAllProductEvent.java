package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Product;

import java.util.ArrayList;

public class GetAllProductEvent {
    private ArrayList<Product> products;

    public GetAllProductEvent(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
}
