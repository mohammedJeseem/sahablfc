package com.example.admin.stockmanagementapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.ListCategoryAdapter;
import com.example.admin.stockmanagementapp.Adapters.SaleGroupListAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetSaleGroupTask;
import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.DeleteBasketEvent;
import com.example.admin.stockmanagementapp.Events.GetAllBasketEvent;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.GetBasketEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.AppPrefs;
import com.example.admin.stockmanagementapp.Utils.BasketManager;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.databinding.ActivitySaleGroupBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class SaleGroupActivity extends AppCompatActivity {

    ActivitySaleGroupBinding mBinding;
    Basket mSaleGroup;
    String groupName;


    SaleGroupListAdapter mAdapter;
    ArrayList<Basket> saleGroupArray = new ArrayList<>();

    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_group);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_sale_group);

        mAdapter = new SaleGroupListAdapter(this);
        mBinding.listSaleBasket.setAdapter(mAdapter);
        mBinding.listSaleBasket.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        mBinding.listSaleGroupBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        getSaleGroup();
//        setRefreshLayout();
//        setAddProductFAB();
    }

    private void setRefreshLayout() {

        mBinding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSaleGroup();
            }
        });
    }

    private void getSaleGroup() {
        GetSaleGroupTask task = new GetSaleGroupTask();
        task.execute();

    }

    @Override
    protected void onResume() {
        super.onResume();

        getSaleGroup();
        setRefreshLayout();
        setAddProductFAB();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void setAddProductFAB() {
        mBinding.addSaleGroupFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSaleGroupNameAlert();
            }
        });
    }


    private void showSaleGroupNameAlert () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter Sale Group Name");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                groupName = (input.getText().toString());

                try {
                    saveSaleGroup();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        builder.show();
    }

    private void saveSaleGroup() throws InterruptedException {

        if(groupName.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please enter group Name",Toast.LENGTH_LONG).show();
            return;
        }
        mSaleGroup = new Basket(groupName);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Long id = AppConstants.getApplicationDB().daoAccess().insertBasket(mSaleGroup);
                Log.d("Basket", "run:id "+id);

                if (id != null) {
                    mSaleGroup.setBasketId(id.intValue());
                    BasketManager.getBasketManager().addBasket(mSaleGroup);
                }
            }
        });
        thread.start();
        thread.join();

        Toast.makeText(getApplicationContext(),"Group Added",Toast.LENGTH_LONG).show();
        getSaleGroup();
    }


    @Subscribe
    public void onGetBasketEvent(GetAllBasketEvent event) {
        if (event.getBaskets().size() < 1) {
            mBinding.msgNoSaleGroup.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Category", Toast.LENGTH_LONG).show();
        } else {
            mBinding.msgNoSaleGroup.setVisibility(View.GONE);
        }
        mBinding.refreshLayout.setRefreshing(false);
        mAdapter.setProducts(event.getBaskets());

        initializeBasketManager(event.getBaskets());
    }

    @Subscribe
    public void onDeleteBasketEvent(final DeleteBasketEvent event) {

        showAlertView(event);
    }

    @Subscribe
    public void onBasketSelected (GetBasketEvent event) {

        Log.d("Vishnu", "onBasketSelected: "+event.getBasket().getName()+" "+event.getBasket().getBasketId());


        BasketManager.getBasketManager().setCurrentBasketIndexById(event.getBasket().getBasketId());

        Intent intent = new Intent(this,SaleActivity.class);
        intent.putExtra(AppConstants.SALE_TYPE, true);
        startActivity(intent);
        finish();

    }


    private void initializeBasketManager (ArrayList<Basket> baskets) {

        BasketManager.getBasketManager().setBaskets(baskets);

        for (Basket basket : baskets) {
            ArrayList<Product> basketAddedItems = AppPrefs.getInstance(this).getSaleGroupBasketItems(basket.getName());
            for (Product mProduct : basketAddedItems) {
                basket.addProductToCart(mProduct);
            }
        }
    }

    private boolean showAlertView(final DeleteBasketEvent event) {

        alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Delete "+event.getBasketForDelete().getName());

        alertDialog.setMessage("Are you sure want to delete this table.");

        alertDialog.setCancelable(true);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                deleteBasket(event);
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

            }
        });

        alertDialog.show();

        return true;
    }

    private void deleteBasket (final DeleteBasketEvent event) {
        final int[] result = new int[1];

        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    result[0] = AppConstants.getApplicationDB().daoAccess().deleteBasket(event.getBasketForDelete());

                    BasketManager.getBasketManager().clearBasketById(event.getBasketForDelete().getBasketId());
                }
            });
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        getSaleGroup();
        Toast.makeText(getApplicationContext(),"Group deleted "+result[0],Toast.LENGTH_SHORT).show();
    }


}
