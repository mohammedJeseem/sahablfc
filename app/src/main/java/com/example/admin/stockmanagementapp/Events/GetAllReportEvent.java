package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;

import java.util.ArrayList;
import java.util.List;

public class GetAllReportEvent {
    private List<SaleReportInfo> saleReport;

    public GetAllReportEvent(List<SaleReportInfo> saleReport) {
        this.saleReport = saleReport;
    }

    public List<SaleReportInfo> getReport() {
        return saleReport;
    }
}
