package com.example.admin.stockmanagementapp;

import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.CategoryArrayAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListReportBillwiseAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListReportProductAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListSaleProductAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.DeleteSaleTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetProductsTask;
import com.example.admin.stockmanagementapp.AsyncTasks.GetReportTask;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Events.DBChangeEvent;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.GetAllProductEvent;
import com.example.admin.stockmanagementapp.Events.GetAllReportEvent;
import com.example.admin.stockmanagementapp.Events.GetBillDetailsEvent;
import com.example.admin.stockmanagementapp.Events.GetProductEvent;
import com.example.admin.stockmanagementapp.Models.SaleBillReportInfo;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;
import com.example.admin.stockmanagementapp.PdfUtils.FileUtils;
import com.example.admin.stockmanagementapp.PdfUtils.PermissionsActivity;
import com.example.admin.stockmanagementapp.PdfUtils.PermissionsChecker;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.ActivityReportBinding;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.example.admin.stockmanagementapp.PdfUtils.LogUtils.LOGE;
import static com.example.admin.stockmanagementapp.PdfUtils.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.example.admin.stockmanagementapp.PdfUtils.PermissionsChecker.REQUIRED_PERMISSION;

public class ReportActivity extends AppCompatActivity {

    ActivityReportBinding mBinding;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    Boolean fromDate,toDate;
    //ListSaleProductAdapter mAdapter;
    ListReportProductAdapter mAdapter;
    ListReportBillwiseAdapter billwiseAdapter;
    private static final int MENU_PDF_ID = 401;
    Long timeStampFromDate = 0L,timeStampToDate;
    PermissionsChecker checker;
    ArrayList<SaleReportInfo> saleReportInfos;
    ArrayList<SaleReportInfo> saleReportCategoryInfo;
    ArrayList<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_report);

        setReportTypeSpinner();
        setAdapterForSelectedSpinnerType();

        getCategory();
        setRefreshLayout();
        initDatePicker();
        handleButtonClicks();

        //getReportByTime(0000l, System.currentTimeMillis()/1000);
    }

    private void getReportByTime(long startTime, long endTime) {
        GetReportTask getReportTask = new GetReportTask(this);
        getReportTask.execute(startTime,endTime);
    }


    private void getCategory() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }

    private void setCategorySpinner(ArrayList<Category> categories) {

        Category emptyCategoryItem = new Category("--All Category--");
        emptyCategoryItem.setCategoryId(-1);
        categories.add(0, emptyCategoryItem);

        ArrayAdapter<Category> adapter =
                new CategoryArrayAdapter(this, categories);

        mBinding.reportCategoryTypeSpinner.setAdapter(adapter);
        //mBinding.saleCategorySpinner.setSelection(getCategorySelectPosition(categories));

        mBinding.reportCategoryTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int category_id = ((Category)mBinding.reportCategoryTypeSpinner.getSelectedItem()).getCategoryId();

                Log.d("Vishnu", "onItemSelected: "+((Category)mBinding.reportCategoryTypeSpinner.getSelectedItem()).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void handleButtonClicks() {

        mBinding.fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
                fromDate = true;
                toDate = false;
            }
        });
        mBinding.toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
                fromDate = false;
                toDate = true;
            }
        });
        mBinding.backButtonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initDatePicker() {
        //Date Picker Definition
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if(fromDate){
                    //Set 00 hours
                    myCalendar.set(Calendar.HOUR_OF_DAY, 0);
                    myCalendar.set(Calendar.MINUTE, 0);
                    myCalendar.set(Calendar.SECOND, 0);
                    myCalendar.set(Calendar.MILLISECOND, 0);
                }else{
                    //Set 24 hours
                    myCalendar.set(Calendar.HOUR_OF_DAY, 23);
                    myCalendar.set(Calendar.MINUTE, 59);
                    myCalendar.set(Calendar.SECOND, 59);
                    myCalendar.set(Calendar.MILLISECOND, 0);
                }


                updateLabel();
            }
        };

    }

    private void setReportTypeSpinner() {

        ArrayList<String> reportType = new ArrayList<>();
        reportType.add("Day");
        reportType.add("Bill");
        reportType.add("Product");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.custom_category_spinner_item, reportType);
        mBinding.reportTypeSpinner.setAdapter(dataAdapter);

        mBinding.reportTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                setAdapterForSelectedSpinnerType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void setAdapterForSelectedSpinnerType(){

        if(mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Day")){

            Toast.makeText(this, "Daywise report", Toast.LENGTH_LONG).show();
            billwiseAdapter = new ListReportBillwiseAdapter(this);
            mBinding.listProductReport.setAdapter(billwiseAdapter);
            mBinding.listProductReport.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        }
        else if(mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Bill")){

            Toast.makeText(this, "Billwise report", Toast.LENGTH_LONG).show();
            billwiseAdapter = new ListReportBillwiseAdapter(this);
            mBinding.listProductReport.setAdapter(billwiseAdapter);
            mBinding.listProductReport.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        }
        else {

            Toast.makeText(this, "product report", Toast.LENGTH_LONG).show();
            mAdapter = new ListReportProductAdapter(this);
            mBinding.listProductReport.setAdapter(mAdapter);
            mBinding.listProductReport.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        }

    }

    public void showPopupReport(View v) {
        PopupMenu popup = new PopupMenu(ReportActivity.this, v);
        popup.getMenu().add(1, MENU_PDF_ID, 1, "Convert PDF");
//        popup.getMenu().add(1, 103, 1, "Menu3");
//        popup.getMenu().add(1, 104, 1, "Menu3");
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case MENU_PDF_ID:
                        startPDFConvert();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void startPDFConvert() {
        checker = new PermissionsChecker(ReportActivity.this);
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        DateFormat timeFormat = new SimpleDateFormat("dd-MM-YYYY");
        Date d = new Date();
        String todayDate = timeFormat.format(d);

        if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
            PermissionsActivity.startActivityForResult(ReportActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
        } else {
            createPdf(FileUtils.getAppPath(ReportActivity.this) +todayDate+"("+ts+")"+".pdf");
        }
    }

    public void createPdf(String dest) {

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));

            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("Android School");
            document.addCreator("Pratik Butani");

            /***
             * Variables for further use....
             */
            BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDetailsTitleChunk = new Chunk("Report Details", mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Font mOrderIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderIdChunk = new Chunk("Date", mOrderIdFont);
            Paragraph mOrderIdParagraph = new Paragraph(mOrderIdChunk);
            document.add(mOrderIdParagraph);

            String dateFrom = mBinding.fromDate.getText().toString();
            String dateTo = mBinding.toDate.getText().toString();
            Font mOrderIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderIdValueChunk = new Chunk("From: "+dateFrom+" To: "+dateTo, mOrderIdValueFont);
            Paragraph mOrderIdValueParagraph = new Paragraph(mOrderIdValueChunk);
            document.add(mOrderIdValueParagraph);

            // Adding Line Breakable Space....
            document.add(new Paragraph(""));
            // Adding Horizontal Line...
            document.add(new Chunk(lineSeparator));
            // Adding Line Breakable Space....
            document.add(new Paragraph(""));

            if(mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Bill")){

                float totalCategoryAmount;

                for(int i = 1; i < categories.size(); i++){

                    //Check category type is selected
                    if (((Category)mBinding.reportCategoryTypeSpinner.getSelectedItem()).getCategoryId() != -1){

                       if (!((Category) mBinding.reportCategoryTypeSpinner.getSelectedItem()).getName().equals(categories.get(i).getName())){
                            continue;
                       }
                    }

                    totalCategoryAmount = 0;

                    Chunk categoryValueChunk = new Chunk(categories.get(i).getName(), mOrderIdValueFont);
                    Paragraph categoryValueParagraph = new Paragraph(categoryValueChunk);
                    document.add(categoryValueParagraph);
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    // Adding Horizontal Line...
                    document.add(new Chunk(lineSeparator));
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));

                    for(int j = 0; j < saleReportCategoryInfo.size(); j++){

                        if(categories.get(i).getName().equals(saleReportCategoryInfo.get(j).category_name)) {

                            long timeStamp = saleReportCategoryInfo.get(j).getTimestamp() * 1000;
                            DateFormat timeFormat = new SimpleDateFormat("dd/MM/YYYY(HH:mm:ss)");
                            Date d = new Date(timeStamp);
                            Font mReportIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
                            Chunk mReportIdChunk = new Chunk((timeFormat.format(d)), mReportIdFont);
                            Paragraph mReportIdParagraph = new Paragraph(mReportIdChunk);
                            document.add(mReportIdParagraph);

                            Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);

                            totalCategoryAmount = totalCategoryAmount + saleReportCategoryInfo.get(j).getTotal_amount();
                            String price = " Rs" + saleReportCategoryInfo.get(j).getTotal_amount();
                            Chunk mReportIdValueChunk = new Chunk( "Bill No :" + saleReportCategoryInfo.get(j).getBill_no()+ " - " + price, mReportIdValueFont);
                            Paragraph mReportIdValueParagraph = new Paragraph(mReportIdValueChunk);
                            document.add(mReportIdValueParagraph);

                            // Adding Line Breakable Space....
                            document.add(new Paragraph(""));
                            // Adding Horizontal Line...
//                        document.add(new Chunk(lineSeparator));
                            // Adding Line Breakable Space....
//                        document.add(new Paragraph(""));

                        }
                    }

                    document.add(new Chunk(lineSeparator));

                    Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                    Chunk mtotalValueChunk = new Chunk( "Total Amount :" + totalCategoryAmount, mReportIdValueFont);
                    Paragraph mtotalIdValueParagraph = new Paragraph(mtotalValueChunk);
                    document.add(mtotalIdValueParagraph);

                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    // Adding Horizontal Line...
                    document.add(new Chunk(lineSeparator));
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    document.add(new Paragraph(""));
                }

            } else if(mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Day")){

                float totalCategoryAmount;
                float perDayTotalAmount;
                String currentDate;

                for(int i = 1; i < categories.size(); i++){

                    //Check category type is selected
                    if (((Category)mBinding.reportCategoryTypeSpinner.getSelectedItem()).getCategoryId() != -1){

                        if (!((Category) mBinding.reportCategoryTypeSpinner.getSelectedItem()).getName().equals(categories.get(i).getName())){
                            continue;
                        }
                    }


                    totalCategoryAmount = 0;
                    perDayTotalAmount = 0;
                    currentDate = new String();

                    Chunk categoryValueChunk = new Chunk(categories.get(i).getName(), mOrderIdValueFont);
                    Paragraph categoryValueParagraph = new Paragraph(categoryValueChunk);
                    document.add(categoryValueParagraph);
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    // Adding Horizontal Line...
                    document.add(new Chunk(lineSeparator));
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));

                    ArrayList<SaleReportInfo> dayArray = new ArrayList<>();

                    for(int l = 0; l < saleReportCategoryInfo.size(); l++) {
                        if(categories.get(i).getName().equals(saleReportCategoryInfo.get(l).category_name)) {

                            dayArray.add(saleReportCategoryInfo.get(l));
                        }
                    }


                    Boolean print;
                    Boolean repeat = null;
                    for(int j = 0; j < dayArray.size(); j++){

                        Log.d("jesy",String.valueOf(dayArray.get(j).getTotal_amount()));
                        long timeStamp = dayArray.get(j).getTimestamp() * 1000;
                        DateFormat timeFormat = new SimpleDateFormat("dd/MM/YYYY");
                        Date d = new Date(timeStamp);

                        if(j == 0) {
                            currentDate = timeFormat.format(d);
                        }

                        Log.d("jesy",currentDate);
                        if(currentDate.equals(timeFormat.format(d))){

                            perDayTotalAmount = perDayTotalAmount + dayArray.get(j).getTotal_amount();
                            print = false;

                            if(j == dayArray.size()-1) {
                                currentDate = timeFormat.format(d);
                                print = true;
                                repeat = true;
                            }
                        } else {

                            repeat = false;
                            print = true;
                        }

                        if(print) {

                            Log.d("jesy data",currentDate+"-"+perDayTotalAmount);

                            Font mReportIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
                            Chunk mReportIdChunk = new Chunk(currentDate, mReportIdFont);
                            Paragraph mReportIdParagraph = new Paragraph(mReportIdChunk);
                            document.add(mReportIdParagraph);

                            Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);


                            String price = " Rs" + dayArray.get(j).getTotal_amount();
                            Chunk mReportIdValueChunk = new Chunk( "Per Day Sale Amount : - " + perDayTotalAmount, mReportIdValueFont);
                            Paragraph mReportIdValueParagraph = new Paragraph(mReportIdValueChunk);
                            document.add(mReportIdValueParagraph);

                                // Adding Line Breakable Space....
                            document.add(new Paragraph(""));

                            perDayTotalAmount = 0;
                            currentDate = timeFormat.format(d);
                            perDayTotalAmount = perDayTotalAmount + dayArray.get(j).getTotal_amount();
                        }

                        if ((j == dayArray.size()-1) && (!repeat)) {

                            Log.d("jesy data",currentDate+"-"+perDayTotalAmount);

                            Font mReportIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
                            Chunk mReportIdChunk = new Chunk(currentDate, mReportIdFont);
                            Paragraph mReportIdParagraph = new Paragraph(mReportIdChunk);
                            document.add(mReportIdParagraph);

                            Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);


                            String price = " Rs" + dayArray.get(j).getTotal_amount();
                            Chunk mReportIdValueChunk = new Chunk( "Per Day Sale Amount : - " + perDayTotalAmount, mReportIdValueFont);
                            Paragraph mReportIdValueParagraph = new Paragraph(mReportIdValueChunk);
                            document.add(mReportIdValueParagraph);

                            // Adding Line Breakable Space....
                            document.add(new Paragraph(""));

                            perDayTotalAmount = 0;
                        }

                        totalCategoryAmount = totalCategoryAmount + dayArray.get(j).getTotal_amount();

                            // Adding Horizontal Line...
//                        document.add(new Chunk(lineSeparator));
                            // Adding Line Breakable Space....
//                        document.add(new Paragraph(""));


                    }

                    document.add(new Chunk(lineSeparator));

                    Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                    Chunk mtotalValueChunk = new Chunk( "Total Amount :" + totalCategoryAmount, mReportIdValueFont);
                    Paragraph mtotalIdValueParagraph = new Paragraph(mtotalValueChunk);
                    document.add(mtotalIdValueParagraph);

                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    // Adding Horizontal Line...
                    document.add(new Chunk(lineSeparator));
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    document.add(new Paragraph(""));
                }

            } else {

                for(int i = 0; i < saleReportInfos.size(); i++) {

                    Font mReportIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
                    Chunk mReportIdChunk = new Chunk("" + saleReportInfos.get(i).getProduct_name(), mReportIdFont);
                    Paragraph mReportIdParagraph = new Paragraph(mReportIdChunk);
                    document.add(mReportIdParagraph);

                    Font mReportIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                    String qtyWg = saleReportInfos.get(i).getQuantity() + "" + saleReportInfos.get(i).getUnit();
                    String amount = String.valueOf("Rs " + saleReportInfos.get(i).getTotal_amount());
                    Chunk mReportIdValueChunk = new Chunk(qtyWg + " - " + amount, mReportIdValueFont);
                    Paragraph mReportIdValueParagraph = new Paragraph(mReportIdValueChunk);
                    document.add(mReportIdValueParagraph);

                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                    // Adding Horizontal Line...
                    document.add(new Chunk(lineSeparator));
                    // Adding Line Breakable Space....
                    document.add(new Paragraph(""));
                }
            }



      /*      // Fields of Order Details...
            // Adding Chunks for Title and value
            Font mOrderIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderIdChunk = new Chunk("Order No:", mOrderIdFont);
            Paragraph mOrderIdParagraph = new Paragraph(mOrderIdChunk);
            document.add(mOrderIdParagraph);

            Font mOrderIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderIdValueChunk = new Chunk("#123123", mOrderIdValueFont);
            Paragraph mOrderIdValueParagraph = new Paragraph(mOrderIdValueChunk);
            document.add(mOrderIdValueParagraph);

            // Adding Line Breakable Space....
            document.add(new Paragraph(""));
            // Adding Horizontal Line...
            document.add(new Chunk(lineSeparator));
            // Adding Line Breakable Space....
            document.add(new Paragraph(""));

            // Fields of Order Details...
            Font mOrderDateFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderDateChunk = new Chunk("Order Date:", mOrderDateFont);
            Paragraph mOrderDateParagraph = new Paragraph(mOrderDateChunk);
            document.add(mOrderDateParagraph);

            Font mOrderDateValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDateValueChunk = new Chunk("06/07/2017", mOrderDateValueFont);
            Paragraph mOrderDateValueParagraph = new Paragraph(mOrderDateValueChunk);
            document.add(mOrderDateValueParagraph);

            document.add(new Paragraph(""));
            document.add(new Chunk(lineSeparator));
            document.add(new Paragraph(""));

            // Fields of Order Details...
            Font mOrderAcNameFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderAcNameChunk = new Chunk("Account Name:", mOrderAcNameFont);
            Paragraph mOrderAcNameParagraph = new Paragraph(mOrderAcNameChunk);
            document.add(mOrderAcNameParagraph);

            Font mOrderAcNameValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderAcNameValueChunk = new Chunk("Pratik Butani", mOrderAcNameValueFont);
            Paragraph mOrderAcNameValueParagraph = new Paragraph(mOrderAcNameValueChunk);
            document.add(mOrderAcNameValueParagraph);

            document.add(new Paragraph(""));
            document.add(new Chunk(lineSeparator));
            document.add(new Paragraph(""));
      */
            document.close();

            Toast.makeText(ReportActivity.this, "Created... :)", Toast.LENGTH_SHORT).show();

            FileUtils.openFile(ReportActivity.this, new File(dest));

        } catch (IOException | DocumentException ie) {
            LOGE("createPdf: Error " + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(ReportActivity.this, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    public Date atEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public Date atStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }


    private void openDatePicker(){
        new DatePickerDialog(ReportActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "dd/MMM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(fromDate){
            mBinding.fromDate.setText(sdf.format(myCalendar.getTime()));
            timeStampFromDate = myCalendar.getTimeInMillis() / 1000;
        }
        if(toDate){
            mBinding.toDate.setText(sdf.format(myCalendar.getTime()));
            timeStampToDate = myCalendar.getTimeInMillis() / 1000;
            getReportByTime(timeStampFromDate, timeStampToDate);
        }
    }

    private void setRefreshLayout() {
        mBinding.refreshLayoutReport.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //getReportByTime(0000l, System.currentTimeMillis()/1000);
            }
        });
    }

    private void getProducts() {
        GetProductsTask task = new GetProductsTask(this);
        task.execute();
    }

    private ArrayList<SaleReportInfo> sortProducts(ArrayList<SaleReportInfo> reports) {
        ArrayList<SaleReportInfo> sortedProducts = new ArrayList<>();
        ArrayList<SaleReportInfo> temp = new ArrayList<>();
        for(int i = 0; i < reports.size(); i++) {
            SaleReportInfo item = getSaleItem(temp, reports.get(i).getProduct_id());
            if (item != null) {
                float quantity = item.getQuantity() + reports.get(i).getQuantity();
                float total_amount = item.getUnit_price() * quantity;
                item.setQuantity(quantity);
                item.setTotal_amount(total_amount);
            } else {
                float total_amount = reports.get(i).getUnit_price() * reports.get(i).getQuantity();
                reports.get(i).setTotal_amount(total_amount);
                temp.add(reports.get(i));
            }
        }

        return temp;
    }

    private SaleReportInfo getSaleItem(ArrayList<SaleReportInfo> saleReportInfos, int id) {

        for (SaleReportInfo saleReportInfo: saleReportInfos) {
            if (saleReportInfo.getProduct_id() == id) {
                return saleReportInfo;
            }
        }
        return null;
    }

    private ArrayList<SaleReportInfo> sortBillWiseReport(ArrayList<SaleReportInfo> reports) {
        ArrayList<Float> productAmounts = new ArrayList<>();
        ArrayList<SaleReportInfo> temp = new ArrayList<>();
        for(int i = 0; i < reports.size(); i++) {

            Log.d("jeseem product", reports.get(i).getProduct_name() +"  price :"+reports.get(i).getUnit_price() +" quatity :"+reports.get(i).getQuantity());

            SaleReportInfo item = getSaleBillItem(temp, reports.get(i));

//
            if (item != null) {
                Log.d("jeseem item product", item.getProduct_name() +"  price :"+item.getUnit_price() +" quatity :"+item.getQuantity());
//                float quantity = item.getQuantity() + reports.get(i).getQuantity();
//                float total_amount = item.getUnit_price() * quantity;
//                item.setQuantity(quantity);
//                item.setTotal_amount(total_amount);

//                total_amount = item.getQuantity() * item.getUnit_price();
//                productAmounts.add(total_amount);
//
//                Log.d("Jes", String.valueOf(total_amount));
//                float totAmount = 0;
//                for(Float amount: productAmounts){
//                    totAmount = totAmount + amount;
//                }
//                item.setTotal_amount(totAmount);

                for(SaleReportInfo tempItem: temp) {

                    if(tempItem.getBill_no() .equals(item.bill_no)) {

                        float totalamount = tempItem.getTotal_amount() + (item.getQuantity() * item.getUnit_price());
                        tempItem.setTotal_amount(totalamount);
                    }
                }


                Log.d("jeseem item product", item.getProduct_name() +"  price :"+item.getTotal_amount() +" quatity :"+item.getQuantity());

            } else {
                float total_amount = reports.get(i).getUnit_price() * reports.get(i).getQuantity();
                reports.get(i).setTotal_amount(total_amount);
                temp.add(reports.get(i));
            }
        }

        return temp;
    }

    private SaleReportInfo getSaleBillItem(ArrayList<SaleReportInfo> saleReportInfos, SaleReportInfo item) {

//        ArrayList<SaleReportInfo> billInfos = new ArrayList<>();

        for (SaleReportInfo saleReportInfo: saleReportInfos) {
            if (saleReportInfo.getBill_no().equals(item.bill_no)) {

                return item;
//                if (!billInfos.contains(reportProduct.getProduct_id())){
//                    billInfos.add(reportProduct);
//                }
//
//                Log.d("jes", String.valueOf(saleReportInfo.getTotal_amount()) +"Bill :"+saleReportInfo.getBill_no());
//                Log.d("jes product", String.valueOf(saleReportInfo.getProduct_name()) +"  price :"+saleReportInfo.getUnit_price() +" quatity :"+saleReportInfo.getQuantity());
            }

        }

//        float totalBillAmount = 0;
//
//        for (int i = 0; i < billInfos.size(); i++) {
//
//            SaleReportInfo billInfo = billInfos.get(i);
//
//            Log.d("jes billInfo product", String.valueOf(billInfo.getProduct_name()) +"  price :"+billInfo.getUnit_price() +" quatity :"+billInfo.getQuantity());
//
//            float total = billInfo.getUnit_price() * billInfo.getUnit_price();
//            totalBillAmount = totalBillAmount + total;
//            Log.d("jes tot", String.valueOf(totalBillAmount));
//            if (i == billInfos.size() - 1) {
//
//                billInfo.setTotal_amount(totalBillAmount);
//                return billInfo;
//            }
//        }
        return null;
    }

    private ArrayList<SaleReportInfo> sortBillWiseReportWithTotalAmount(ArrayList<SaleReportInfo> reports) {

        String billNo;
        ArrayList<SaleReportInfo> resultArray = new ArrayList<>();
        float totalAmount;

        for(int i = 0; i < reports.size(); i++) {

            totalAmount = 0;
            billNo = reports.get(i).getBill_no();

            for(int j = i; j < reports.size(); j++) {

                if(reports.get(j).bill_no.equals(billNo)) {

                    totalAmount += (reports.get(j).getQuantity() * reports.get(j).getUnit_price());

                }
            }

            if (!isBillNoAlreadyExists(resultArray, reports.get(i).getBill_no())) {
                reports.get(i).setTotal_amount(totalAmount);
//                SaleReportInfo saleItem = new SaleReportInfo("0000",100563166, 400, 2, 0, "Name","kg",100,"","");
                SaleReportInfo saleItem = new SaleReportInfo(reports.get(i).getBill_no(),
                                            reports.get(i).getTimestamp(),reports.get(i).getTotal_amount(), reports.get(i).getQuantity(), reports.get(i).getProduct_id(),
                                            reports.get(i).getProduct_name(), reports.get(i).getUnit(), reports.get(i).getUnit_price(), reports.get(i).getCategory_name(), reports.get(i).getImage_path());

                resultArray.add(saleItem);
            }

        }


        return resultArray;
    }

    private boolean isBillNoAlreadyExists (ArrayList<SaleReportInfo> billInfos, String billNo) {

        for(SaleReportInfo item : billInfos) {

            if (item.getBill_no().equals(billNo))
                return true;
        }

        return false;
    }

    private ArrayList<SaleReportInfo> sortBillWiseReportWithCategory(ArrayList<SaleReportInfo> reports) {
        ArrayList<SaleReportInfo> sortedProducts = new ArrayList<>();
        ArrayList<SaleReportInfo> temp = new ArrayList<>();
        for(int i = 0; i < reports.size(); i++) {
            SaleReportInfo item = getSaleBillItemCategory(temp, reports.get(i));
//            Log.d("jeseemProduct",reports.get(i).getProduct_name() +"  price :"+reports.get(i).getUnit_price() +" quatity :"+reports.get(i).getQuantity());
            if (item != null) {


                float total_amount = item.getTotal_amount() + (item.getUnit_price() * item.getQuantity());
                item.setTotal_amount(total_amount);


            } else {
                float total_amount = reports.get(i).getUnit_price() * reports.get(i).getQuantity();
                reports.get(i).setTotal_amount(total_amount);

                SaleReportInfo saleItem = new SaleReportInfo(reports.get(i).getBill_no(),
                        reports.get(i).getTimestamp(),reports.get(i).getTotal_amount(), reports.get(i).getQuantity(), reports.get(i).getProduct_id(),
                        reports.get(i).getProduct_name(), reports.get(i).getUnit(), reports.get(i).getUnit_price(), reports.get(i).getCategory_name(), reports.get(i).getImage_path());
                temp.add(saleItem);
            }
        }

        return temp;
    }

    private SaleReportInfo getSaleBillItemCategory(ArrayList<SaleReportInfo> saleReportInfos, SaleReportInfo info) {

        for (SaleReportInfo saleReportInfo: saleReportInfos) {
            if (saleReportInfo.getBill_no().equals(info.getBill_no()) && saleReportInfo.getCategory_name().equals(info.getCategory_name())) {

                saleReportInfo.setQuantity(info.getQuantity());
                saleReportInfo.setUnit_price(info.getUnit_price());
                return saleReportInfo;
            }
        }
        return null;
    }

    private ArrayList<SaleReportInfo> sortReportCategory(ArrayList<SaleReportInfo> reports) {
        ArrayList<SaleReportInfo> sortedProducts = new ArrayList<>();
        ArrayList<SaleReportInfo> temp = new ArrayList<>();
        for(int i = 0; i < reports.size(); i++) {
            SaleReportInfo item = getSaleCategory(temp, reports.get(i).getCategory_name());
            if (item != null) {

                sortedProducts.add(item);
            } else {

                temp.add(reports.get(i));
            }
        }

        return temp;
    }

    private SaleReportInfo getSaleCategory(ArrayList<SaleReportInfo> saleReportInfos, String category) {

        for (SaleReportInfo saleReportInfo: saleReportInfos) {
            if (saleReportInfo.getCategory_name().equals(category)) {
                return saleReportInfo;
            }
        }
        return null;
    }

    private void confirmDeleteBill (final String billNo) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Delete bill no : #"+billNo);

        alertDialog.setMessage("Confirm delete bill no : #"+billNo);

        alertDialog.setCancelable(true);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                deleteBill(billNo);

            } });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

            }
        });

        alertDialog.show();
    }

    private void deleteBill (String billNo) {

        DeleteSaleTask deleteSaleTask = new DeleteSaleTask();
        deleteSaleTask.execute(billNo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Subscribe
    public void onGetReportEvent(GetAllReportEvent event) {
        if (event.getReport().size() < 1) {
            Toast.makeText(this, "No Products", Toast.LENGTH_LONG).show();
        }
        mBinding.refreshLayoutReport.setRefreshing(false);

        if(mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Bill") || mBinding.reportTypeSpinner.getSelectedItem().toString().equals("Day")){

            saleReportInfos = sortBillWiseReportWithTotalAmount((ArrayList<SaleReportInfo>) event.getReport());
//            saleReportInfos = sortBillWiseReportWithCategory((ArrayList<SaleReportInfo>) event.getReport());

            // sorting array in descending by bill number
            ArrayList<SaleReportInfo> sortedSalesInfo = saleReportInfos;
            Collections.sort(sortedSalesInfo, new Comparator<SaleReportInfo>() {
                @Override
                public int compare(SaleReportInfo s1, SaleReportInfo s2) {
                    return Integer.compare(Integer.parseInt(s2.getBill_no()), Integer.parseInt(s1.getBill_no()) );
                }
            });
            billwiseAdapter.setBillWiseProducts(sortedSalesInfo);

            saleReportCategoryInfo = sortBillWiseReportWithCategory((ArrayList<SaleReportInfo>) event.getReport());
//            saleReportCategoryInfo = sortBillWiseCategoryReportWithTotalAmount((ArrayList<SaleReportInfo>) event.getReport());

        } else {

            saleReportInfos = sortProducts((ArrayList<SaleReportInfo>) event.getReport());
            mAdapter.setProducts(saleReportInfos);
        }
    }

    @Subscribe
    public void onCategoryLoaded(GetAllCategoryEvent event) {

        categories = event.getCategories();
        setCategorySpinner(event.getCategories());
    }

    @Subscribe
    public void onListItemClickedEvent(GetBillDetailsEvent event) {

        if (event.getIsLongClickEvent()) {

            confirmDeleteBill(event.getBillNo());

        } else {

            Intent intent = new Intent(this, BillDetailsActivity.class);
            intent.putExtra("billNo", event.getBillNo());
            startActivity(intent);
        }
    }

    @Subscribe
    public void onProductChange(DBChangeEvent event) {
        if (event.getType() == DBChangeEvent.SALE_DELETE) {

            if (event.isSuccess()) {
                //getReportByTime(0000l, System.currentTimeMillis()/1000);
                Toast.makeText(this, "Sale bill deleted", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
