package com.example.admin.stockmanagementapp.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

//@Entity(foreignKeys = @ForeignKey(entity = Category.class,
//            parentColumns = "categoryId", // in Reference class
//            childColumns = "categoryId",  // in Same class
//            onDelete = CASCADE),
//        indices = {@Index("categoryId")})

@Entity(foreignKeys = @ForeignKey(
        entity = Category.class,
        parentColumns = "categoryId",
        childColumns = "categoryId",
        onDelete = CASCADE  ),
        indices = {@Index("categoryId")})

public class Product implements Serializable{

    @PrimaryKey(autoGenerate = true)
    private int productId;
    private int categoryId;
    private String name;
    private String image_path;
    private String unit;
    private Float price;
    private String category_name;

    @Ignore
    private float quantity;

    public Product(int categoryId, String name, String image_path, String unit, Float price) {
        this.categoryId = categoryId;
        this.name = name;
        this.image_path = image_path;
        this.unit = unit;
        this.price = price;

        this.quantity = 1f;
    }

    public Product getProduct() {
        return this;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Float getPrice() {
        return price;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
}
