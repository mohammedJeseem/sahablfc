package com.example.admin.stockmanagementapp.AsyncTasks;

import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.DBChangeEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

public class DeleteSaleTask extends AsyncTask<String, Void, Boolean> {

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        EventBus.getDefault().post(new DBChangeEvent(aBoolean,DBChangeEvent.SALE_DELETE
        ));
    }

    @Override
    protected Boolean doInBackground(String... billNos) {
        int id = AppConstants.getApplicationDB().daoAccess().deleteSaleByBillNo(billNos[0]);
        if (id > 0) {
            return true;
        }
        return false;
    }
}
