package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.CartItemRemovedEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.LayoutSaleCartProductCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ListCartProductAdapter extends RecyclerView.Adapter<ListCartProductAdapter.ProductViewHolder> {

    private ArrayList<Product> mProducts = null;
    private Context mContext;
    TextWatcher textWatcher = null;
    LayoutSaleCartProductCardBinding bindingView;

    public ListCartProductAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ListCartProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        bindingView = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_sale_cart_product_card, parent, false);

        return new ListCartProductAdapter.ProductViewHolder(bindingView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListCartProductAdapter.ProductViewHolder holder, final int position) {

        final Product product = mProducts.get(position);
//        final LayoutSaleCartProductCardBinding bindingView = holder.getLayoutBinding();
        holder.configQuantityET(product.getUnit());
        holder.setPosition(position);

        // bindingView.productCategoryTv.setText(mProducts.get(position).getCategoryId());
        bindingView.saleCartProductNameTv.setText(mProducts.get(position).getName());

        Glide.with(mContext).load(product.getImage_path())
                .apply(new RequestOptions())
                .into(bindingView.saleCartProductImgView);
        bindingView.saleCartProductPriceTv.setText(Utility.getPriceFormated(product.getPrice()));
        String weight = String.valueOf(product.getQuantity()+" "+product.getUnit());
        bindingView.saleCartProductWeightTv.setText(weight);
        //bindingView.saleCategoryTv.setText(product.getCategory_name());

        if (product.getUnit().equals(AppConstants.Unit.KILOGRAM)) {
            bindingView.saleCartProductQtyCount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            bindingView.saleCartIncrementCount.setVisibility(View.GONE);
            bindingView.saleCartDecrementCount.setVisibility(View.GONE);
            bindingView.saleCartProductQtyCount.setText(String.valueOf(mProducts.get(position).getQuantity()));
        } else if (product.getUnit().equals(AppConstants.Unit.NUMBER)) {
            bindingView.saleCartProductQtyCount.setInputType(InputType.TYPE_CLASS_NUMBER);
            bindingView.saleCartProductQtyCount.setText(String.valueOf((int)mProducts.get(position).getQuantity()));
        }
        bindingView.saleCartProductQtyCount.setText(String.valueOf(holder.getItem_quantity()));

        bindingView.saleCartDeleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                EventBus.getDefault().post(new CartItemRemovedEvent(product.getProductId()));
//                bindingView.deletedView.setVisibility(View.VISIBLE);


                CartAddedItem.getCartAddedItem().removeProductFromCart(product.getProductId());
                EventBus.getDefault().post(new OnCartItemChanged());
                mProducts.remove(product);
                notifyDataSetChanged();

            }
        });

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(".") && !s.toString().equals("")) {
                    holder.updateItemPrice();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
//        bindingView.saleCartProductQtyCount.addTextChangedListener(textWatcher);
    }
    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setProducts(ArrayList<Product> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private LayoutSaleCartProductCardBinding mBinding;
        private int position;
        private float item_quantity = 1;

        public ProductViewHolder(LayoutSaleCartProductCardBinding view) {
            super(view.getRoot());
            mBinding = view;

            mBinding.saleCartIncrementCount.setOnClickListener(this);
            mBinding.saleCartDecrementCount.setOnClickListener(this);
//            setCounterTextWatcher();

        }

        public LayoutSaleCartProductCardBinding getLayoutBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.sale_cart_increment_count:
                    incrementCounter();
                    break;

                case R.id.sale_cart_decrement_count:
                    decrementCounter();
                    break;

                default:
                    break;
            }
        }

        private void configQuantityET(String unit) {
            if (unit.equals(AppConstants.Unit.KILOGRAM)) {
                mBinding.saleCartProductQtyCount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                mBinding.saleCartIncrementCount.setVisibility(View.GONE);
                mBinding.saleCartDecrementCount.setVisibility(View.GONE);
                mBinding.saleCartProductQtyCount.setText(String.valueOf((int)mProducts.get(position).getQuantity()));
            } else if (unit.equals(AppConstants.Unit.NUMBER)) {
                mBinding.saleCartProductQtyCount.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        }

        private void setCounterTextWatcher() {
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(".") && !s.toString().equals("")) {
                        updateItemPrice();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };
//            mBinding.saleCartProductQtyCount.addTextChangedListener(textWatcher);
        }

        private void setPosition(int position) {
            this.position = position;
        }

        private void incrementCounter() {
            int currentQuantity = Integer.parseInt(mBinding.saleCartProductQtyCount.getText().toString()) + 1;
            mBinding.saleCartProductQtyCount.setText(String.valueOf(currentQuantity));
            mProducts.get(position).setQuantity(currentQuantity);
//            CartAddedItem.getCartAddedItem().setCartItemQuantity(position, currentQuantity);
        }

        private void decrementCounter() {
            int currentQuantity = Integer.parseInt(mBinding.saleCartProductQtyCount.getText().toString()) - 1;
            int count = Math.max(1, currentQuantity);
            mBinding.saleCartProductQtyCount.setText(String.valueOf(count));
            mProducts.get(position).setQuantity(currentQuantity);
//            CartAddedItem.getCartAddedItem().setCartItemQuantity(position, currentQuantity);
        }

        public void updateItemPrice() {
            String quantityText = mBinding.saleCartProductQtyCount.getText().toString();
            float price;
            if (quantityText.equals("")) {
                price = 1f;
            } else {
                price = Float.parseFloat(quantityText) * mProducts.get(position).getPrice();
            }

            CartAddedItem.getCartAddedItem().setCartItemQuantity(mProducts.get(position).getProductId(), Float.parseFloat(quantityText));
            EventBus.getDefault().post(new OnCartItemChanged());
            mBinding.saleCartProductPriceTv.setText(Utility.getPriceFormated(price));
        }

        public float getItem_quantity() {
            return item_quantity;
        }

        public void setItem_quantity(float item_quantity) {
            this.item_quantity = item_quantity;
        }
    }
}
