package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Utils.AppPrefs;
import com.example.admin.stockmanagementapp.databinding.ActivityShopSettingsBinding;

public class ShopSettings extends AppCompatActivity {

    ActivityShopSettingsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_settings);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_shop_settings);

        setLastValue();
        mBinding.backButtonShopSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.saveShopSesttingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSaveButton();
            }
        });

    }

    private void setLastValue() {
        mBinding.shopSettingName.setText(AppPrefs.getInstance(this).getShopName());
        mBinding.shopSettingPhone.setText(AppPrefs.getInstance(this).getShopPhone());
        mBinding.shopSettingAddress.setText(AppPrefs.getInstance(this).getShopAddress());
        mBinding.shopSettingGst.setText(AppPrefs.getInstance(this).getShopGST());
        mBinding.shopSettingTinNumber.setText(AppPrefs.getInstance(this).getShopTin());
    }

    private void handleSaveButton() {

        String shopName = mBinding.shopSettingName.getText().toString();
        String shopPhone = mBinding.shopSettingPhone.getText().toString();
        String shopAddress = mBinding.shopSettingAddress.getText().toString();
        String shopGst = mBinding.shopSettingGst.getText().toString();
        String shopTin = mBinding.shopSettingTinNumber.getText().toString();

        if(shopName.isEmpty() || shopPhone.isEmpty() || shopAddress.isEmpty() || shopGst.isEmpty()){
           Toast.makeText(getApplicationContext(),"Enter the Valid Values",Toast.LENGTH_LONG).show();
            return;
        }
        AppPrefs.getInstance(this).saveShopSettings(shopName,shopPhone,shopAddress,shopGst,shopTin);

        mBinding.shopSettingName.setText("");
        mBinding.shopSettingPhone.setText("");
        mBinding.shopSettingAddress.setText("");
        mBinding.shopSettingGst.setText("");
        mBinding.shopSettingTinNumber.setText("");
        Toast.makeText(getApplicationContext(),"Successfully Saved",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
