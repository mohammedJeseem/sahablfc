package com.example.admin.stockmanagementapp;

import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.CartItemListAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListCartPreviewAdapter;
import com.example.admin.stockmanagementapp.Adapters.ListCartProductAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.AsyncTasks.SaveSaleItemTask;
import com.example.admin.stockmanagementapp.Entities.Basket;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Entities.Sale;
import com.example.admin.stockmanagementapp.Entities.SaleItem;
import com.example.admin.stockmanagementapp.Events.CartItemRemovedEvent;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.OnCartItemChanged;
import com.example.admin.stockmanagementapp.Events.OnSaleSavedEvent;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.AppPrefs;
import com.example.admin.stockmanagementapp.Utils.BasketManager;
import com.example.admin.stockmanagementapp.Utils.CartAddedItem;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.ActivitySaleCartBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class CartActivity extends AppCompatActivity {

    ActivitySaleCartBinding mBinding;
    ListCartProductAdapter mAdapter;
    LinearLayoutManager layoutManager;
    ListCartPreviewAdapter mPreviewAdapter;
    ArrayList<Product> mProducts = new ArrayList<>();
    ArrayList<Product> cartProducts;
    ArrayList<Product> products;
    private static final int MENU_PRINT_ID = 301;
    private static final int MENU_CLEAR_CART_ID = 302;
    boolean previewShowing = false;
    String bill_no;
    private Float mDiscount = 0f;
    boolean isCounterBill = false;

    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;

    OutputStream outputStream = null;
    InputStream inputStream;
    Thread thread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    AlertDialog alertDialog;

    Boolean isSaleTypeGroup;
    ArrayList<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_cart);

        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_sale_cart);
//        mAdapter = new ListCartProductAdapter(this);
        mPreviewAdapter = new ListCartPreviewAdapter();

//        mBinding.listProductSaleCart.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
//        mBinding.listProductSaleCart.setLayoutManager(layoutManager);

//        getAddedProducts();
        mBinding.backButtonSaleCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.saleCartSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSaveButton();
//                showAlert();
            }
        });

        if (getIntent().hasExtra(AppConstants.SALE_TYPE) && getIntent().getExtras().getBoolean(AppConstants.SALE_TYPE)) {
            isSaleTypeGroup = true;
        } else {
            isSaleTypeGroup = false;
        }

        getCategory();

        updateTotalPrice();

        getCartList();

    }


    private void getCartList() {

        if (isSaleTypeGroup) {
            products = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
            ArrayList<Product> basketCartItems = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleGroupBasketItems(basketCartItems, BasketManager.getBasketManager().getCurrentBasket().getName());
        } else {
            products = CartAddedItem.getCartAddedItem().getAddedProducts();
            ArrayList<Product> cartItems = CartAddedItem.getCartAddedItem().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleCartItems(cartItems);
        }

        if (products == null) {
            products = new ArrayList<>();
        }
        CartItemListAdapter adapter =  new CartItemListAdapter(products,getApplicationContext(),isSaleTypeGroup);
        mBinding.testList.setAdapter(adapter);
    }

    private void handleSaveButton() {
        if(previewShowing) {
            //saveAndBill();
            showPrintConfirmationAlert();

        } else {
            showDiscountAlert();
//            previewPayment();
        }
    }

    private void getCategory() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }

    private void showPrintConfirmationAlert () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("PRINT or SAVE");
        builder.setMessage("Do you need to print the bill");

// Set up the buttons
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                saveSaleinDB();
            }
        });
        builder.setNegativeButton("Print", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                saveAndBill();
            }
        });

        builder.show();
    }

    private void showDiscountAlert () {

        if (isSaleTypeGroup) {
            cartProducts = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
        } else {
            cartProducts = CartAddedItem.getCartAddedItem().getAddedProducts();
        }

        if(cartProducts == null || cartProducts.size() < 1) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter discount in %");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().toString().length() > 0) {
                    mDiscount = Float.parseFloat(input.getText().toString());

                } else {
                    mDiscount = 0.0f;
                }
                previewPayment();
            }
        });
        builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                previewPayment();
            }
        });

        builder.show();
    }

    private void previewPayment() {

        if (isSaleTypeGroup) {
            cartProducts = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
        } else {
            cartProducts = CartAddedItem.getCartAddedItem().getAddedProducts();
        }

        bill_no = AppPrefs.getInstance(this).getLastBillNo().toString();
        if(cartProducts == null || cartProducts.size() < 1) return;

        previewShowing = true;

        updatePriceDetails();

        mBinding.cartPreviewBillNo.setText("Bill No : "+bill_no);
        mBinding.cartPreviewRl.setVisibility(View.VISIBLE);
        mBinding.listProductSaleCart.setVisibility(View.GONE);
        mBinding.testList.setVisibility(View.GONE);
        mBinding.saleCartSaveButton.setText(R.string.save_and_bill);
        mBinding.saleCartSaveButton.setBackgroundResource(R.color.limeColor);
        mPreviewAdapter.setProducts(cartProducts);
        mBinding.cartPreviewList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mBinding.cartPreviewList.setAdapter(mPreviewAdapter);
    }

    String custName = "";
    private void saveAndBill() {
        showProgressBar(true);

        isCounterBill = false;
        if(outputStream != null) {
            custName = mBinding.cartPreviewCustName.getText().toString();
            printData();
        } else {
            connectPrinter();
        }
    }

    private void connectPrinter() {
        try{
            findBluetoothDevice();
//            openBluetoothPrinter();
//            printData();

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }
    }

    void findBluetoothDevice(){

        try{

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(bluetoothAdapter==null){
                handlePrinterError("Bluetooth is disabled");
            }
            if(!bluetoothAdapter.isEnabled()){
//                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBT,0);

                handlePrinterError("Bluetooth is disabled");
            }

            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();

            if(pairedDevice.size()>0){
                for(BluetoothDevice pairedDev:pairedDevice){

                    // My Bluetoth printer name is BTP_F09F1A
                    if(pairedDev.getName().equals(AppPrefs.getInstance(this).getPrinterName())){
                        bluetoothDevice=pairedDev;

                        openBluetoothPrinter();
                        break;
                    }
                }
            } else {

                handlePrinterError("No paired device found");
            }

        }catch(Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }

    }

    // Open Bluetooth Printer

    void openBluetoothPrinter() throws IOException {
        try{

            //Standard uuid from string //
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStream=bluetoothSocket.getOutputStream();

            inputStream=bluetoothSocket.getInputStream();
            beginListenData();


            if(outputStream != null) {
                if(isCounterBill) {
                    printCounterBillData();
                } else {
                    printData();
                }
            } else {
                handlePrinterError("Couldn't connect printer");
            }

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't connect printer");
        }
    }
    void beginListenData(){
        try{

            final Handler handler =new Handler();
            final byte delimiter=10;
            stopWorker =false;
            readBufferPosition=0;
            readBuffer = new byte[1024];

            thread=new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimiter){
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer,0,
                                                encodedByte,0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte,"US-ASCII");
                                        readBufferPosition=0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Log.d("beginListenData", data);
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                    }
                                }
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                        }
                    }

                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }



    // Printing Text to Bluetooth Printer  ZKC//
    void printDataForZKC(){
        saveSaleinDB();
        try{
//            outputStream.flush();
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };

//            // Bold
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nbold".getBytes());
//
//            // Height
//            format[2] = ((byte)(0x10 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nheight".getBytes());
//
//            // Width
//            format[2] = ((byte) (0x20 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nwidth".getBytes());
//
//            // Underline
//            format[2] = ((byte)(0x80 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nUnderline".getBytes());
//
//            // Small
//            format[2] = ((byte)(0x1 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nsmall".getBytes());



            byte[] PRINT_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
            byte[] PRINT_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
            byte[] PRINT_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };

            outputStream.write(PRINT_ALIGN_CENTER);
            String companyDetails = "\n"+AppPrefs.getInstance(this).getShopName() +
                    "\n"+AppPrefs.getInstance(this).getShopAddress() +
                    "\n"+AppPrefs.getInstance(this).getShopPhone()+" \n";
            outputStream.write(companyDetails.getBytes());

            String custNam = "\nName : "+custName+"\n";
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(custNam.getBytes());

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String dateAndTime = "\n"+formatter.format(date);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(dateAndTime.getBytes());

            String bill_num = "\nBill NO : "+bill_no+"\n";
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(bill_num.getBytes());

            //String line_seperator = "\n----------------------------\n";
            String line_seperator = "--------------------------------";
            //outputStream.write(line_seperator.getBytes());
            String header = line_seperator+"\nItem \t Rate \t Qty \t Amnt \n"+line_seperator;
            outputStream.write(header.getBytes());
            //outputStream.write(line_seperator.getBytes());
            outputStream.write(PRINT_ALIGN_RIGHT);
            for(int i = 0; i < cartProducts.size(); i++){
                String name = cartProducts.get(i).getName();
                String rate = String.valueOf(cartProducts.get(i).getPrice());
                String quantity = String.valueOf(cartProducts.get(i).getQuantity());
                String amount = String.format("%.2f", (cartProducts.get(i).getPrice() * cartProducts.get(i).getQuantity()));
                String  data = "\n"+name+"\n"+"\t"+rate+"\t "+quantity+"\t "+amount+"\n";
                /*if(name.length() > 6){
                    data = "\n"+name+"\n"+"\t"+rate+"\t "+quantity+"\t "+amount+"\n";
                }else{
                    data = "\n"+name+"\t "+rate+"\t "+quantity+"\t "+amount+"\n";
                }*/
                outputStream.write(PRINT_ALIGN_LEFT);
                outputStream.write(data.getBytes());
            }

            String total_price;
            if (isSaleTypeGroup) {
                total_price = line_seperator+"\nTOTAL : Rs " + String.format("%.2f", BasketManager.getBasketManager().getCurrentBasket().getTotalprice()) +"\n"+line_seperator;
            } else {
                total_price = line_seperator+"\nTOTAL : Rs " + String.format("%.2f", CartAddedItem.getCartAddedItem().getTotalprice()) +"\n"+line_seperator;
            }

            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(total_price.getBytes());


            outputStream.write(PRINT_ALIGN_CENTER);
            String msg = "\n *** THANK YOU ***\nVISIT AGAIN\n\n\n";
            outputStream.write(msg.getBytes());


            outputStream.flush();

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't print");
        }
    }



    // Printing Text to Bluetooth Printer  ZKC//
    void printData(){

        saveSaleinDB();
        try{
//            outputStream.flush();
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };

//            // Bold
//            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nbold".getBytes());
//
//            // Height
//            format[2] = ((byte)(0x10 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nheight".getBytes());
//
//            // Width
//            format[2] = ((byte) (0x20 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nwidth".getBytes());
//
//            // Underline
//            format[2] = ((byte)(0x80 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nUnderline".getBytes());
//
//            // Small
//            format[2] = ((byte)(0x1 | arrayOfByte1[2]));
//            outputStream.write(format);
//            outputStream.write("\nsmall".getBytes());



            byte[] PRINT_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
            byte[] PRINT_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
            byte[] PRINT_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };

            outputStream.write(PRINT_ALIGN_CENTER);
            String companyDetails = "\n" + AppPrefs.getInstance(this).getShopName() + "\n";
            outputStream.write(companyDetails.getBytes());
            outputStream.write(PRINT_ALIGN_CENTER);
            String placeDetails = AppPrefs.getInstance(this).getShopAddress() + "\n";
            outputStream.write(placeDetails.getBytes());
            outputStream.write(PRINT_ALIGN_CENTER);
            String phoneDetails = AppPrefs.getInstance(this).getShopPhone() + "\n";
            outputStream.write(phoneDetails.getBytes());
            outputStream.write(PRINT_ALIGN_LEFT);
            String gstTin = AppPrefs.getInstance(this).getShopTin();
            outputStream.write(gstTin.getBytes());

            String custNam = "\nName : "+mBinding.cartPreviewCustName.getText().toString()+"";
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(custNam.getBytes());

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String dateAndTime = "\n"+formatter.format(date);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(dateAndTime.getBytes());

            String bill_num = "\nBill NO : "+bill_no+"\n";
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(bill_num.getBytes());

            //String line_seperator = "\n----------------------------\n";
            String line_seperator = "-----------------------";
            //outputStream.write(line_seperator.getBytes());
            String header = line_seperator+"\nItem \t Rate \t Qty \t Amnt \n"+line_seperator;
            outputStream.write(header.getBytes());
            //outputStream.write(line_seperator.getBytes());
            outputStream.write(PRINT_ALIGN_RIGHT);
            for(int i = 0; i < cartProducts.size(); i++){
                String name = cartProducts.get(i).getName();
                String rate = String.valueOf(cartProducts.get(i).getPrice());
                String quantity = String.valueOf(cartProducts.get(i).getQuantity());
                String amount = String.format("%.2f", (cartProducts.get(i).getPrice() * cartProducts.get(i).getQuantity()));
                String  data = "\n"+name+"\n";

                String  dummy = "";
                String  data1 = dummy+"\t  "+rate+"\t  "+quantity+"\t  "+amount+"\n";
                /*if(name.length() > 6){
                    data = "\n"+name+"\n"+"\t"+rate+"\t "+quantity+"\t "+amount+"\n";
                }else{
                    data = "\n"+name+"\t "+rate+"\t "+quantity+"\t "+amount+"\n";
                }*/
                outputStream.write(PRINT_ALIGN_LEFT);
                outputStream.write(data.getBytes());
                outputStream.write(PRINT_ALIGN_RIGHT);
                outputStream.write(data1.getBytes());
            }



            String net_tot;
            if (isSaleTypeGroup) {
                net_tot = line_seperator+"\nTOT AMNT : Rs " + String.format("%.2f", BasketManager.getBasketManager().getCurrentBasket().getTotalprice()) +"\n";
            } else {
                net_tot = line_seperator+"\nTOT AMNT : Rs " + String.format("%.2f", CartAddedItem.getCartAddedItem().getTotalprice()) +"\n";
            }

            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(net_tot.getBytes());

            String discount_price = "DISC "+Float.toString(mDiscount)+"% : Rs " + String.format("%.2f", discPrice) +"\n";
            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(discount_price.getBytes());

            String net_price = "NET AMNT : Rs " + String.format("%.2f", netPrice) +"\n";
            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(net_price.getBytes());

            String gst_price = "GST "+ AppPrefs.getInstance(this).getShopGST()+"% : Rs " + String.format("%.2f", gstPrice) +"\n"+line_seperator+"\n";
            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(gst_price.getBytes());

            String total_price = "\nTOTAL : Rs " + String.format("%.2f", finalAmnt) +"\n"+line_seperator+"\n";
            format[2] = ((byte)(0x8 | arrayOfByte1[2]));
            outputStream.write(format);
            outputStream.write(PRINT_ALIGN_LEFT);
            outputStream.write(total_price.getBytes());


            outputStream.write(PRINT_ALIGN_CENTER);
            String msg = " *** THANK YOU ***\n";
            outputStream.write(msg.getBytes());
            String msg1 = "VISIT AGAIN\n\n\n";
            outputStream.write(PRINT_ALIGN_CENTER);
            outputStream.write(msg1.getBytes());


            outputStream.flush();

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't print");
        }
    }

    private String getPrintData() {
        String data;
        data = "\nCompany Name\n" +
                "";

        return data;
    }

    private void handlePrinterError(String toastMsg) {
        showProgressBar(false);
        alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("No Printer attached");

        alertDialog.setMessage("Please connect printer and retry.");

        alertDialog.setCancelable(true);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Retry", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                connectPrinter();

            } });

        if(!isCounterBill) {

            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Save Only", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    saveSaleinDB();

                }});
        }


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

            }});

        alertDialog.show();
        Toast.makeText(CartActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
    }

    private void showProgressBar(boolean show) {
        if (show) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    private void saveSaleinDB() {
        ArrayList<SaleItem> saleItems = new ArrayList<>();
        for (int i = 0; i < cartProducts.size(); i++) {
            SaleItem saleItem = new SaleItem(0, cartProducts.get(i).getProductId(),cartProducts.get(i).getQuantity());
            saleItems.add(saleItem);
        }


        float total_price;
        if (isSaleTypeGroup) {
            total_price = BasketManager.getBasketManager().getCurrentBasket().getTotalprice();
        } else {
            total_price = CartAddedItem.getCartAddedItem().getTotalprice();
        }

        Long tsLong = System.currentTimeMillis()/1000;

        Sale sale = new Sale(bill_no, total_price, tsLong);
        SaveSaleItemTask.Params params = new SaveSaleItemTask.Params(sale, saleItems);

        SaveSaleItemTask task = new SaveSaleItemTask();
        task.execute(params);
    }


    public void showPopupCart(View v) {
        PopupMenu popup = new PopupMenu(this, v);
//        popup.getMenu().add(1, MENU_PRINT_ID, 1, "Printer Test");
        popup.getMenu().add(1, MENU_CLEAR_CART_ID, 1, "Clear Cart");
//        popup.getMenu().add(1, 103, 1, "Menu3");
//        popup.getMenu().add(1, 104, 1, "Menu3");
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case MENU_PRINT_ID:
                        startPrinterActivity();
                        return true;

                    case MENU_CLEAR_CART_ID:
                        if (isSaleTypeGroup) {
                            BasketManager.getBasketManager().getCurrentBasket().clearBasketItem();
                        } else {
                            CartAddedItem.getCartAddedItem().clearCartItem();
                        }

                        updateTotalPrice();
//                        getAddedProducts();
                        getCartList();
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void printCounterBill(View v) {

        isCounterBill = true;
        if(outputStream != null) {
            printCounterBillData();
        } else {
            connectPrinter();
        }

    }

    void printCounterBillData(){

        try{
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };
            byte[] PRINT_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
            byte[] PRINT_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
            byte[] PRINT_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };


            int firstItemInCategory;

            String counterToken = "TOKEN - "+String.valueOf(getCounterTokenNo());

            for(int i = 1; i < categories.size(); i++) {

                firstItemInCategory = 0;

                for (int j = 0; j < products.size(); j++) {

                    if (categories.get(i).getName().equals(products.get(j).getCategory_name())) {

                        if(firstItemInCategory == 0) {

                            firstItemInCategory++;

                            String basketName;
                            if (isSaleTypeGroup) {
                                basketName = "\n" +BasketManager.getBasketManager().getCurrentBasket().getName() +"\n";
                            } else {
                                basketName = "\n" +"----------" + "\n";
                            }

                            outputStream.write(PRINT_ALIGN_CENTER);
                            String categoryType =  "\n" +categories.get(i).getName() + "\n";
                            outputStream.write(categoryType.getBytes());

                            outputStream.write(PRINT_ALIGN_CENTER);
                            outputStream.write(basketName.getBytes());

                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                            Date date = new Date();
                            String dateAndTime = "\n"+formatter.format(date)+"\n";
                            outputStream.write(PRINT_ALIGN_LEFT);
                            outputStream.write(dateAndTime.getBytes());


                            String token = "\n"+counterToken+"\n";
                            outputStream.write(PRINT_ALIGN_LEFT);
                            outputStream.write(token.getBytes());

                            outputStream.write(PRINT_ALIGN_LEFT);
                            String line_seperator = "-----------------------";
                            //outputStream.write(line_seperator.getBytes());
                            String header = line_seperator + "\nItem \t Qty \n" + line_seperator;
                            outputStream.write(header.getBytes());
                            //outputStream.write(line_seperator.getBytes());
                            outputStream.write(PRINT_ALIGN_LEFT);
                        }
                        String name = products.get(j).getName();
                        String quantity = String.valueOf(products.get(j).getQuantity());
                        String data = "\n" + name + "\n";

                        String dummy = "";
                        String data1 = dummy + "\t " + quantity + "\n";

                        outputStream.write(PRINT_ALIGN_LEFT);
                        outputStream.write(data.getBytes());
                        outputStream.write(PRINT_ALIGN_RIGHT);
                        outputStream.write(data1.getBytes());

                    }

                }

                String dummy = "";
                String data1 = "\n" + dummy + "\t  \n";
                format[2] = ((byte) (0x8 | arrayOfByte1[2]));
                outputStream.write(format);
                outputStream.write(PRINT_ALIGN_LEFT);
                outputStream.write(data1.getBytes());


            }
            outputStream.flush();

        }catch (Exception ex){
            ex.printStackTrace();
            handlePrinterError("Couldn't print");
        }
    }


    private long getCounterTokenNo() {


        long token = 1;

        long currentTimeStamp = System.currentTimeMillis() / 1000;

        if(AppPrefs.getInstance(this).getLastDateNo() > currentTimeStamp) {

            token = AppPrefs.getInstance(this).getLastCounterNo();
            AppPrefs.getInstance(this).updateLastCounterNo();

        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());

//getting the desired properties
            calendar.get(Calendar.YEAR);
            calendar.get(Calendar.MONTH);
            calendar.get(Calendar.DAY_OF_MONTH);

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 0);


            long timeStampEndDay = calendar.getTimeInMillis() / 1000;
            AppPrefs.getInstance(this).iniLastDateNo(timeStampEndDay);
            AppPrefs.getInstance(this).clearCounterNo();
            token = AppPrefs.getInstance(this).getLastCounterNo();
            AppPrefs.getInstance(this).updateLastCounterNo();


        }

        return token;

    }

    private void startPrinterActivity() {
//        Intent intent = new Intent(this, PrinterTest.class);
//        startActivity(intent);
//        finish();
    }

    private void getAddedProducts() {
        mProducts = null;
        mProducts = CartAddedItem.getCartAddedItem().getAddedProducts();
//        mAdapter.setProducts(mProducts);
//        mAdapter.notifyDataSetChanged();

        mBinding.listProductSaleCart.setAdapter(null);
        mBinding.listProductSaleCart.setLayoutManager(null);
        mAdapter.setProducts(mProducts);
        mBinding.listProductSaleCart.setAdapter(mAdapter);
        mBinding.listProductSaleCart.setLayoutManager(layoutManager);
        mAdapter.notifyDataSetChanged();

    }

    private void updateTotalPrice() {
        float totalPrice;
        if (isSaleTypeGroup) {
            totalPrice = BasketManager.getBasketManager().getCurrentBasket().getTotalprice();
            ArrayList<Product> basketItems = BasketManager.getBasketManager().getCurrentBasket().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleGroupBasketItems(basketItems, BasketManager.getBasketManager().getCurrentBasket().getName());
        } else {
            totalPrice = CartAddedItem.getCartAddedItem().getTotalprice();
            ArrayList<Product> cartItems = CartAddedItem.getCartAddedItem().getAddedProducts();
            AppPrefs.getInstance(this).saveSaleCartItems(cartItems);

        }
        mBinding.saleCartTotalAmountTv.setText(Utility.getPriceFormated(totalPrice));
    }

    Float netPrice = 0.0f;
    Float discPrice = 0.0f;
    Float gstPrice = 0.0f;
    Float finalAmnt = 0.0f;
    private void updatePriceDetails() {

        float totalPrice;
        if (isSaleTypeGroup) {
            totalPrice = BasketManager.getBasketManager().getCurrentBasket().getTotalprice();
        } else {
            totalPrice = CartAddedItem.getCartAddedItem().getTotalprice();
        }

        mBinding.saleCartGrossAmountTv.setText(Utility.getPriceFormated(totalPrice));

        String gstRateString = "GST @ "+ AppPrefs.getInstance(this).getShopGST()+"%";
        mBinding.saleCartGstRateTv.setText(gstRateString);

        discPrice = totalPrice * (mDiscount) / 100;
        finalAmnt = totalPrice - discPrice;
        mBinding.saleCartDiscRateTv.setText("DISCOUNT @ "+mDiscount+"%");
        mBinding.saleCartDiscAmountTv.setText("-"+Utility.getPriceFormated(discPrice));

        float gstRate = 0 ;
        try {
            gstRate = Float.parseFloat(AppPrefs.getInstance(this).getShopGST());
            if (gstRate > 0) {
                gstPrice = finalAmnt * (gstRate / (100 + gstRate));
                mBinding.saleCartGstAmountTv.setText(Utility.getPriceFormated(gstPrice));

                netPrice = finalAmnt / (1 + (gstRate / 100));
                mBinding.saleCartNetAmountTv.setText(Utility.getPriceFormated(netPrice));
            }
        } catch (NumberFormatException ex) {
            // Not a float
        }

        mBinding.saleCartTotalAmountTv.setText(Utility.getPriceFormated(finalAmnt));

        mBinding.saleCartTaxDetail.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        if (previewShowing) {
            mBinding.saleCartSaveButton.setText(R.string.proceed_payment);
            mBinding.saleCartSaveButton.setBackgroundResource(R.color.orangeColor);
//            mBinding.listProductSaleCart.setVisibility(View.VISIBLE);
            mBinding.testList.setVisibility(View.VISIBLE);
            mBinding.cartPreviewRl.setVisibility(View.GONE);
            mBinding.saleCartTaxDetail.setVisibility(View.INVISIBLE);
            updateTotalPrice();

            previewShowing = false;
        } else {
            super.onBackPressed();
//            Intent intent = new Intent(this, SaleActivity.class);
//            startActivity(intent);
//            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (outputStream != null) {
            try {
                outputStream.close();
                outputStream = null;
            } catch (IOException ex){

            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
                inputStream = null;
            } catch (IOException ex){

            }
        }
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
                bluetoothSocket = null;
            } catch (IOException ex){

            }
        }
        stopWorker=true;
    }

    @Subscribe
    public void OnCartChangedEvent(OnCartItemChanged event) {
        updateTotalPrice();
    }

    @Subscribe
    public void onSaleItemSaved(OnSaleSavedEvent event) {
        if (event.isSuccess()) {
            if (isSaleTypeGroup) {
                BasketManager.getBasketManager().getCurrentBasket().clearBasketItem();
                AppPrefs.getInstance(this).clearSaleGroupBasketItems(BasketManager.getBasketManager().getCurrentBasket().getName());
            } else {
                CartAddedItem.getCartAddedItem().clearCartItem();
                AppPrefs.getInstance(this).clearSaveSaleCartItems();
            }
            AppPrefs.getInstance(this).updateLastBillNo();


            Toast.makeText(this,"Sale saved", Toast.LENGTH_SHORT).show();
//            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this,"Sale not saved", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onCartItemRemoved(CartItemRemovedEvent event) {
//        CartAddedItem.getCartAddedItem().removeProductFromCart(event.getProduct_id());
//        updateTotalPrice();
//        getAddedProducts();

        getCartList();
    }

    @Subscribe
    public void onGetProductEvent(GetAllCategoryEvent event) {
        categories = event.getCategories();

    }
}
