package com.example.admin.stockmanagementapp.Models;

import java.util.ArrayList;

public class SaleBillReportInfo {
    public ArrayList<Float> billProductAmountArray;
    public String bill_no;
    public long timestamp;
    public float total_amount = 0;
    public float quantity;
    public int product_id;
    public String product_name;
    public String unit;
    public float unit_price;
    public String category_name;
    public String image_path;

    public SaleBillReportInfo(ArrayList<Float> billProductAmountArray, String bill_no, long timestamp, float total_amount, float quantity, int product_id, String product_name, String unit, float unit_price, String category_name, String image_path) {
        this.billProductAmountArray = billProductAmountArray;
        this.bill_no = bill_no;
        this.timestamp = timestamp;
        this.total_amount = total_amount;
        this.quantity = quantity;
        this.product_id = product_id;
        this.product_name = product_name;
        this.unit = unit;
        this.unit_price = unit_price;
        this.category_name = category_name;
        this.image_path = image_path;
    }

    public ArrayList<Float> getBillProductAmountArray() {
        return billProductAmountArray;
    }

    public void setBillProductAmountArray(Float amount) {
        this.billProductAmountArray.add(amount);
    }
    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(float total_amount) {
        this.total_amount = total_amount;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(float unit_price) {
        this.unit_price = unit_price;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
