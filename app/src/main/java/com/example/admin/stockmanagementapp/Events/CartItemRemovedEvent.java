package com.example.admin.stockmanagementapp.Events;

public class CartItemRemovedEvent {
    private int product_id;

    public CartItemRemovedEvent(int product_id) {
        this.product_id = product_id;
    }

    public int getProduct_id() {
        return product_id;
    }
}
