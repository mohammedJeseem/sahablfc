package com.example.admin.stockmanagementapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.os.Build;
import android.os.Environment;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.PdfUtils.FileUtils;
import com.example.admin.stockmanagementapp.Utils.AppConstants;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.ActivityMainBinding;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.List;

import io.github.yavski.fabspeeddial.FabSpeedDial;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityMainBinding mBinding;

    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        mBinding.productCard.setOnClickListener(this);
        mBinding.saleCard.setOnClickListener(this);
        mBinding.reportCard.setOnClickListener(this);
        mBinding.saleGroupCard.setOnClickListener(this);

        setCoverHeight();
//        setCardHeight();
//        test();
        setFabMenuAction();
    }

    private void setFabMenuAction() {
        mBinding.fabMainActivity.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                switch(menuItem.getItemId()){
                    case R.id.action_printer:
                        openPrinterConfigActivity();
                        return true;

                    case R.id.action_shop:
                        openShopSettingsActivity();
                        return true;

                    case R.id.action_DB:
                        handleExportImport();
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void onMenuClosed() {

            }
        });
    }

    private void openPrinterConfigActivity() {
      //  Toast.makeText(getApplicationContext(),"Printer",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, PrinterConfig.class);
        startActivity(intent);
        finish();
    }

    private void openShopSettingsActivity() {
        //Toast.makeText(getApplicationContext(),"Settings",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, ShopSettings.class);
        startActivity(intent);
        finish();

    }

    private void setCoverHeight() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }
        int width = size.x;
        int height = size.y;

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBinding.coverImageContainer.getLayoutParams();
        params.height = (int) (0.25 * height);
        mBinding.coverImageContainer.setLayoutParams(params);

        int cardWidth = (int) (width - Utility.dpToPx(this, 16));
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mBinding.productCard.getLayoutParams();
        layoutParams.height = cardWidth/2;

        mBinding.productCard.setLayoutParams(layoutParams);
        mBinding.saleCard.setLayoutParams(layoutParams);
        mBinding.reportCard.setLayoutParams(layoutParams);
        mBinding.saleGroupCard.setLayoutParams(layoutParams);
    }

    public void onClick(View v){

        switch(v.getId()){
            case R.id.product_card:
                openProductActivity();
                break;

            case R.id.sale_card:
                openSaleActivity();
                break;

            case R.id.report_card:
                openReportActivity();
                break;

            case R.id.sale_group_card:
//                openCartActivity();
                saleGroupActivity();
                break;

            default:
                break;
        }
    }

    private void openCartActivity() {
        Intent intent = new Intent(this,CartActivity.class);
        startActivity(intent);
        //finish();
    }

    private void saleGroupActivity() {

        Intent intent = new Intent(this,SaleGroupActivity.class);
        startActivity(intent);
//        finish();
    }

    private void openProductActivity() {
        Intent intent = new Intent(this,ListProductActivity.class);
        startActivity(intent);
        finish();
    }

    private void openSaleActivity() {
        Intent intent = new Intent(this,SaleActivity.class);
        startActivity(intent);
        finish();
    }
    private void openReportActivity() {
        Intent intent = new Intent(this,ReportActivity.class);
        startActivity(intent);
        finish();
    }
    private void test() {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Category category = new Category("test category");
                AppConstants.getApplicationDB().daoAccess().insertCategory(category);

                List<Category> list = AppConstants.getApplicationDB().daoAccess().fetchAllCategories();
                Log.d("Vishnu", "test: "+list.get(0));
            }
        });
        thread.start();

    }


    private void handleExportImport() {

        alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Export/Import DB");

        alertDialog.setMessage("Please select below options for db export or import.");

        alertDialog.setCancelable(true);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Export", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                if(isStoragePermissionGranted()) {
                    exportDB();
                }
            } });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

            }});

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Import", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                if(isStoragePermissionGranted()) {
                    importDB();
                }
            }});

        alertDialog.show();
    }


    private void importDB() {
        // TODO Auto-generated method stub

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data  = Environment.getDataDirectory();

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {

                String  currentDBPath= "//data//" + getPackageName()
                        + "//databases//" + AppConstants.getDBName();
                String backupDBPath  = "/BackupFolder/"+"Backup-"+AppConstants.getDBName();
                File  backupDB= new File(data, currentDBPath);
                File currentDB  = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Toast.makeText(getBaseContext(), backupDB.toString(),
                        Toast.LENGTH_LONG).show();

                restartApp();

            }
        } catch (Exception e) {

            Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }



    //exporting database
    private void exportDB() {
        // TODO Auto-generated method stub

        File direct = new File(Environment.getExternalStorageDirectory() + "/BackupFolder");

        if(!direct.exists())
        {
            if(direct.mkdir())
            {
                //directory is created;
            }

        }

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {

                Log.d("Test", "sdcard mounted and writable");
                String  currentDBPath= "//data//" + getPackageName()
                        + "//databases//" + AppConstants.getDBName();
                String backupDBPath  = "/BackupFolder/" +"Backup-"+AppConstants.getDBName();
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(getBaseContext(), backupDB.toString(),
                        Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

            Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }

    // restart application
    private void restartApp () {

        Intent restartIntent = new Intent(this, SplashActivity.class);
        restartIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //Set this flag
        startActivity(restartIntent);
        finish();
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Storage","Permission is granted");
                return true;
            } else {

                Log.v("Storage","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Storage","Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v("Storage","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission

            Toast.makeText(getApplicationContext(),"Storage Permission Granted",Toast.LENGTH_LONG).show();
        }
    }

}
