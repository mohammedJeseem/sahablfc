package com.example.admin.stockmanagementapp.Adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.LayoutCartPreviewItemBinding;

import java.util.ArrayList;

public class ListCartPreviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Product> mProducts = null;


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutCartPreviewItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_cart_preview_item, parent, false);
        return new CartPreviewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Product product = mProducts.get(position);
        LayoutCartPreviewItemBinding bindingView = ((CartPreviewViewHolder)holder).getLayoutBinding();

        bindingView.productName.setText(product.getName());
        bindingView.unitPrice.setText(String.valueOf(product.getPrice()));
        bindingView.quantity.setText(String.valueOf(product.getQuantity()));

        float amount = product.getQuantity() * product.getPrice();
        bindingView.amount.setText(Utility.getPriceFormated(amount));
    }

    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    public void setProducts(ArrayList<Product> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class CartPreviewViewHolder extends RecyclerView.ViewHolder{
        private LayoutCartPreviewItemBinding mBinding;

        public CartPreviewViewHolder(LayoutCartPreviewItemBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutCartPreviewItemBinding getLayoutBinding() {
            return mBinding;
        }

    }
}
