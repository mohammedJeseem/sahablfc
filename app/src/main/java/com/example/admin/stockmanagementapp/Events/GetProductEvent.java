package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Product;

public class GetProductEvent {
    private Product product;

    public GetProductEvent(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }
}
