package com.example.admin.stockmanagementapp.Events;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Models.SaleBillProduct;

import java.util.ArrayList;
import java.util.List;

public class GetBillProductsEvents {

    private List<SaleBillProduct> products;

    public GetBillProductsEvents(List<SaleBillProduct> products) {
        this.products = products;
    }

    public List<SaleBillProduct> getProducts() {
        return products;
    }

}
