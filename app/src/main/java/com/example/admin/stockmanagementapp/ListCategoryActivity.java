package com.example.admin.stockmanagementapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.admin.stockmanagementapp.Adapters.ListCategoryAdapter;
import com.example.admin.stockmanagementapp.AsyncTasks.GetCategoryTask;
import com.example.admin.stockmanagementapp.Entities.Category;
import com.example.admin.stockmanagementapp.Events.GetAllCategoryEvent;
import com.example.admin.stockmanagementapp.Events.RefreshListEvent;
import com.example.admin.stockmanagementapp.databinding.ActivityListCategoryBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class ListCategoryActivity extends AppCompatActivity {

    ActivityListCategoryBinding mBinding;
    ListCategoryAdapter mAdapter;
    ArrayList<Category> mCategories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_list_category);
        mAdapter = new ListCategoryAdapter(this);
        mBinding.listCategory.setAdapter(mAdapter);
        mBinding.listCategory.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        getCategory();
        setRefreshLayout();

        Toolbar myToolbar = mBinding.listCategoryToolbar;
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        myToolbar.setTitle("List Category");
        setSupportActionBar(myToolbar);

        setAddCategoryFAB();
    }

    private void setRefreshLayout() {
        mBinding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCategory();
            }
        });
    }
    private void setAddCategoryFAB() {
        mBinding.addCategotyFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),AddCategory.class);
//                intent.putExtra("add_product", true);
                startActivity(intent);
                finish();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
    private void getCategory() {
        GetCategoryTask task = new GetCategoryTask();
        task.execute();
    }

        @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onGetProductEvent(GetAllCategoryEvent event) {
        if (event.getCategories().size() < 1) {
            mBinding.msgNoCategory.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Category", Toast.LENGTH_LONG).show();
        } else {
            mBinding.msgNoCategory.setVisibility(View.GONE);
        }
        mBinding.refreshLayout.setRefreshing(false);
        mAdapter.setProducts(event.getCategories());
    }

    @Subscribe(sticky = true)
    public void onRefreshEvent(RefreshListEvent event) {
        if (event.getType() == RefreshListEvent.REFRESH_CATEGORY) {
            getCategory();
        }
    }
}
