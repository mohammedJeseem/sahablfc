package com.example.admin.stockmanagementapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetProductEvent;
import com.example.admin.stockmanagementapp.R;
import com.example.admin.stockmanagementapp.Utils.Utility;
import com.example.admin.stockmanagementapp.databinding.LayoutProductCardBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ListProductAdapter extends RecyclerView.Adapter<ListProductAdapter.ProductViewHolder> {

    private ArrayList<Product> mProducts = null;
    private Context mContext;

    public ListProductAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutProductCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_product_card, parent, false);

        return new ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        final Product product = mProducts.get(position);
        LayoutProductCardBinding bindingView = ((ProductViewHolder)holder).getLayoutBinding();

       // bindingView.productCategoryTv.setText(mProducts.get(position).getCategoryId());
        bindingView.productNameTv.setText(mProducts.get(position).getName());
        Glide.with(mContext).load(product.getImage_path())
                .apply(new RequestOptions())
                .into(bindingView.productImgView);
//        String price = product.getPrice().toString() + " Rs";
        bindingView.productPriceTv.setText(Utility.getPriceFormated(product.getPrice()));
        bindingView.productCategoryTv.setText(product.getCategory_name());

        ((ProductViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new GetProductEvent(product));
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size();
        else
            return 0;
    }

    public void setProducts(ArrayList<Product> mProducts) {
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{
        private LayoutProductCardBinding mBinding;

        public ProductViewHolder(LayoutProductCardBinding view) {
            super(view.getRoot());
            mBinding = view;
        }

        public LayoutProductCardBinding getLayoutBinding() {
            return mBinding;
        }

    }
}
