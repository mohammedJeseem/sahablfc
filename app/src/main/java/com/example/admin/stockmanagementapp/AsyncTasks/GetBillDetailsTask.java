package com.example.admin.stockmanagementapp.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.example.admin.stockmanagementapp.Entities.Product;
import com.example.admin.stockmanagementapp.Events.GetAllReportEvent;
import com.example.admin.stockmanagementapp.Events.GetBillProductsEvents;
import com.example.admin.stockmanagementapp.Models.SaleBillProduct;
import com.example.admin.stockmanagementapp.Models.SaleReportInfo;
import com.example.admin.stockmanagementapp.Utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class GetBillDetailsTask extends AsyncTask<String, Void, List<SaleBillProduct>> {

    Context mContext;

    public GetBillDetailsTask(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    protected List<SaleBillProduct> doInBackground(String... strings) {

        List<SaleBillProduct> list = AppConstants.getApplicationDB().daoAccess().getProductByBillNo(strings[0]);
        return list;
    }

    @Override
    protected void onPostExecute(List<SaleBillProduct> products) {
        super.onPostExecute(products);
        EventBus.getDefault().post(new GetBillProductsEvents(products));
    }
}
